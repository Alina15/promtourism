package com.sez.promtourism.PromTourism.controller.rest;

import com.sez.promtourism.PromTourism.dto.AddOrDeleteTourDto;
import com.sez.promtourism.PromTourism.dto.TourDto;
import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.dto.create.CreateTourRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.service.TourService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/tour")
@RequiredArgsConstructor
@Slf4j
public class TourController {
    private final TourService tourService;


    @RequestMapping(value = "/getTour", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TourDto> getOneTour(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(tourService.getOne(id));
    }
}
