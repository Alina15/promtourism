package com.sez.promtourism.PromTourism.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/about")
public class MvcAboutController {
    @GetMapping
    public String index() {
        return "about/about";
    }
}
