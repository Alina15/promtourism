package com.sez.promtourism.PromTourism.controller.mvc;

import com.sez.promtourism.PromTourism.dto.TypeOfTourDto;
import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.dto.create.CreateUserRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.service.TourService;
import com.sez.promtourism.PromTourism.service.UserService;
import com.sez.promtourism.PromTourism.service.userdetails.CustomUserDetails;
import jakarta.security.auth.message.AuthException;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static com.sez.promtourism.PromTourism.constants.Error.Users.USER_FORBIDDEN_ERROR;

@Controller
@Slf4j
@RequestMapping("/users")
@RequiredArgsConstructor
public class MvcUserController {
    private final UserService userService;
    private final TourService tourService;


    @GetMapping("/registration")
    public String registrationForm(final Model model) {
        model.addAttribute("userForm", new UserDto());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserDto userDto,
                               BindingResult bindingResult) {
        if (userService.getUserByLogin(userDto.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже зарегистрирован");
            return "registration";
        }
        if (userService.getUserByEmail(userDto.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже зарегистрирован");
            return "registration";
        }
        if (userService.getUserByPhone(userDto.getPhone()) != null) {
            bindingResult.rejectValue("phone", "error.phone", "Такой телефон уже зарегистрирован");
            return "registration";
        }
        userService.create(userDto);
        return "redirect:/login";
    }
    @GetMapping("/profile/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException, AuthException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!Objects.isNull(customUserDetails.getId())) {

            if (!id.equals(customUserDetails.getId())) {
                throw new AuthException(HttpStatus.FORBIDDEN + ": " + USER_FORBIDDEN_ERROR);
            }


        }
        UserDto userDto = userService.getOne(id);
        model.addAttribute("userForm", userDto);
        return "profile/updateProfile";
    }

    @PostMapping("/profile/update")
    public String update(@RequestParam("id") Long id,@ModelAttribute("userForm") UserDto userDto) throws IOException, NotFoundException {
        Long idUser = userService.getOne(id).getId();
        userService.update(userDto, idUser);
        return "redirect:/users/profile/" + idUser;
    }

    @GetMapping("/profile/{id}")
    public String userProfileForm(@RequestParam(value = "page", defaultValue = "1") int page,
                                  @RequestParam(value = "size", defaultValue = "4") int pageSize,
                                  @PathVariable Long id,
                                  Model model) throws NotFoundException, AuthException {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "startTour"));
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!Objects.isNull(customUserDetails.getId())) {

                if (!id.equals(customUserDetails.getId())) {
                    throw new AuthException(HttpStatus.FORBIDDEN + ": " + USER_FORBIDDEN_ERROR);
                }


        }
        if (!isAdmin()) {
        model.addAttribute("tours", userService.getTourByUserId(customUserDetails.getId(), pageRequest));
        model.addAttribute("user", userService.getOne(id));
        return "profile/viewUserProfile";
        }else {
                model.addAttribute("tours", userService.getAll(pageRequest));
                model.addAttribute("user", userService.getOne(id));
                return "profile/viewAdminProfile";
        }
    }

    @GetMapping("/listUsersByTour/{id}")
    public String listAllUsers(@PathVariable Long id, Model model) throws NotFoundException {
        List<UserDto> users = tourService.getUsersByTourId(id);
        model.addAttribute("users", users);
        return "users/viewAllUsersByTour";
    }

    private boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                if (authority.getAuthority().equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }
    @GetMapping("/remember-password")
    public String rememberPassword() {
        return "users/rememberPassword";
    }

    @PostMapping("/remember-password")
    public String rememberPassword(@ModelAttribute("changePasswordForm") UserDto userDTO) throws NotFoundException {
        userDTO = userService.getUserByEmail(userDTO.getEmail());
        if (Objects.isNull(userDTO)) {
            return "Error!";
        } else {
            userService.sendChangePasswordEmail(userDTO);
            return "redirect:/login";
        }
    }
    @GetMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 Model model) {
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }

    @PostMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 @ModelAttribute("changePasswordForm") UserDto userDTO) throws NotFoundException {
        userService.changePassword(uuid, userDTO.getPassword());
        return "redirect:/login";
    }

    @GetMapping("/change-password/user")
    public String changePassword(Model model) throws NotFoundException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto userDTO = userService.getOne(customUserDetails.getId());
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        userService.update(userDTO, userDTO.getId());
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }
}

