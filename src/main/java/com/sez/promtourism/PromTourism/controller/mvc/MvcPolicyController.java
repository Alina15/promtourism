package com.sez.promtourism.PromTourism.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/policy")
public class MvcPolicyController {
    @GetMapping("/personalDataProcessingPolicy")
    public String personalDataProcessingPolicy() {
        return "policy/personalDataProcessingPolicy";
    }

    @GetMapping("/privacyPolicy")
    public String privacyPolicy() {
        return "policy/privacyPolicy";
    }
}