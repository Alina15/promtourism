package com.sez.promtourism.PromTourism.controller.mvc;

import com.sez.promtourism.PromTourism.dto.ReviewDto;
import com.sez.promtourism.PromTourism.dto.ReviewWithUserAndTourDto;
import com.sez.promtourism.PromTourism.dto.TourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateReviewRequest;
import com.sez.promtourism.PromTourism.exception.MyDeleteException;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.service.ResidentService;
import com.sez.promtourism.PromTourism.service.ReviewService;
import com.sez.promtourism.PromTourism.service.TourService;
import com.sez.promtourism.PromTourism.service.userdetails.CustomUserDetails;
import jakarta.security.auth.message.AuthException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Objects;

import static com.sez.promtourism.PromTourism.constants.Error.Users.USER_FORBIDDEN_ERROR;
import static com.sez.promtourism.PromTourism.model.Status.COMPLETED;

@Controller
@RequestMapping("/reviews")
public class MvcReviewsController {
    private final ReviewService reviewService;
    private final TourService tourService;
    public MvcReviewsController(ReviewService reviewService, TourService tourService) {
        this.reviewService = reviewService;
        this.tourService = tourService;
    }
    @GetMapping("")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "9") int pageSize,
                          Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "score"));
        Page<ReviewWithUserAndTourDto> reviewWithUserAndTourDtoPage = reviewService.getAllNotDeletedReviewWithUserAndTour(pageRequest);
        model.addAttribute("reviews", reviewWithUserAndTourDtoPage);
        return "reviews/listAllReviews";
    }

    @GetMapping("/add/{id}")
    public String create(@PathVariable Long id,
                         Model model) throws NotFoundException, AuthException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!Objects.isNull(customUserDetails.getId())) {
            TourDto tourDto = tourService.getOne(id);
            if(tourDto.getUserIds().stream().noneMatch(u-> Objects.equals(u, customUserDetails.getId())) || tourDto.getStatus()!=COMPLETED || reviewService.getExistsByUserIdAndTourId(customUserDetails.getId(), tourDto.getId()))
                //if(reviewService.getExistsByUserIdAndTourId(customUserDetails.getId(), tourDto.getId()))
                    throw new AuthException(HttpStatus.FORBIDDEN + ": " + USER_FORBIDDEN_ERROR);
        }
        model.addAttribute("tourId", id);
        return "reviews/createReview";
    }

    @PostMapping("/add")
    public String create(@RequestParam("id") Long id,@ModelAttribute("reviewForm") CreateReviewRequest createReviewRequest) throws IOException, NotFoundException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        createReviewRequest.setUserId(customUserDetails.getId());
        createReviewRequest.setTourId(id);
        createReviewRequest.setResidentId(tourService.getOne(id).getResidentId());
        reviewService.createReviewByRequest(createReviewRequest);
        return "redirect:/reviews";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException {
        ReviewDto reviewDto = reviewService.getOne(id);
        model.addAttribute("review", reviewDto);
        return "reviews/updateReview";
    }

    @PostMapping("/update")
    public String update(@RequestParam("id") Long id,@ModelAttribute("reviewForm") ReviewDto reviewDto) throws IOException, NotFoundException {

        reviewService.update(reviewDto, id);
        return "redirect:/reviews";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException, NotFoundException {
        reviewService.deleteSoft(id);
        return "redirect:/reviews";
    }
}
