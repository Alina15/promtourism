package com.sez.promtourism.PromTourism.controller.mvc;


import com.sez.promtourism.PromTourism.dto.AddOrDeleteTourDto;
import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.TourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateResidentRequest;
import com.sez.promtourism.PromTourism.dto.create.CreateTourRequest;
import com.sez.promtourism.PromTourism.exception.MyDeleteException;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.model.Status;
import com.sez.promtourism.PromTourism.service.ResidentService;
import com.sez.promtourism.PromTourism.service.TourService;
import com.sez.promtourism.PromTourism.service.TypeOfTourService;
import com.sez.promtourism.PromTourism.service.UserService;
import com.sez.promtourism.PromTourism.service.userdetails.CustomUserDetails;
import jakarta.security.auth.message.AuthException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static com.sez.promtourism.PromTourism.constants.Error.Users.USER_FORBIDDEN_ERROR;

@Controller
@RequestMapping("/tours")
public class MvcTourController {
    private final TourService tourService;
    private final ResidentService residentService;
    private final TypeOfTourService typeOfTourService;
    private final UserService userService;
    public MvcTourController(TourService tourService, UserService userService, ResidentService residentService, TypeOfTourService typeOfTourService) {
        this.userService = userService;
        this.tourService = tourService;
        this.residentService = residentService;
        this.typeOfTourService = typeOfTourService;
    }

    @GetMapping("/addUserTour/{id}")
    public String addUserTour(@PathVariable Long id,
                          Model model) throws NotFoundException {
        model.addAttribute("tours", tourService.findAllByResidentId(id));
        return "tours/addUserTour";
    }

    @PostMapping("/addUserTour")
    public String addUserTour(@ModelAttribute("addUserTourForm") AddOrDeleteTourDto addOrDeleteTourDto) throws NotFoundException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        addOrDeleteTourDto.setUserId(customUserDetails.getId());
        tourService.addTour(addOrDeleteTourDto);
        return "redirect:/";
    }
    @GetMapping("/deleteUserTour/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException, NotFoundException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AddOrDeleteTourDto addOrDeleteTourDto = new AddOrDeleteTourDto();
        addOrDeleteTourDto.setUserId(customUserDetails.getId());
        addOrDeleteTourDto.setTourId(id);
        tourService.deleteTourIdByUserId(addOrDeleteTourDto);
        return "redirect:/users/profile/" + customUserDetails.getId();
    }
    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("residents", residentService.listAll());
        model.addAttribute("typeOfTours", typeOfTourService.listAll());
        return "tours/createTour";
    }

    @PostMapping("/add")
    public String create( @ModelAttribute("tourForm") CreateTourRequest createTourRequest) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        tourService.createTourByRequest(createTourRequest);
        return "redirect:/users/profile/" + customUserDetails.getId();
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException {
        TourDto tourDto = tourService.getOne(id);
        model.addAttribute("residents", residentService.listAll());
        model.addAttribute("typeOfTours", typeOfTourService.listAll());
        model.addAttribute("tour", tourDto);
        return "tours/updateTour";
    }

    @PostMapping("/update")
    public String update(@RequestParam("id") Long id,@ModelAttribute("tourForm") TourDto tourDto) throws IOException, NotFoundException {
        tourService.updateTour(tourDto, id);
        return "redirect:/";
    }
    @PostMapping("/search")
    public String searchTour(@RequestParam(value = "page", defaultValue = "1") int page,
                                  @RequestParam(value = "size", defaultValue = "4") int pageSize,
                                  @ModelAttribute("tourSearchForm") TourDto tourDto,
                                  Model model) throws NotFoundException {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "startTour"));
        model.addAttribute("tours", userService.searchTour(tourDto.getStatus(), pageRequest));
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("user", userService.getOne(customUserDetails.getId()));
        return "profile/viewAdminProfile";
    }


}
