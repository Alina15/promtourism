package com.sez.promtourism.PromTourism.controller.mvc;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.ResidentWithReviewDto;
import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.dto.create.CreateResidentRequest;
import com.sez.promtourism.PromTourism.exception.MyDeleteException;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.model.Resident;
import com.sez.promtourism.PromTourism.service.ResidentService;
import jakarta.security.auth.message.AuthException;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.modeler.BaseAttributeFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.sez.promtourism.PromTourism.constants.Error.Users.USER_FORBIDDEN_ERROR;

@Slf4j
@Controller
@RequestMapping("/residents")
public class MvcResidentController {
    private final ResidentService residentService;
    @Value("${file.upload-dir}")
    private String uploadDir;
    public MvcResidentController(ResidentService residentService) {
        this.residentService = residentService;
    }
    @GetMapping("/images/{imageName}")
    @ResponseBody
    public byte[] getImage(@PathVariable String imageName) throws IOException {
        Path imagePath = Paths.get(uploadDir+"/"+ imageName);
        return Files.readAllBytes(imagePath);
    }
    @GetMapping("/gallery/{imageFlour}/{imageName}")
    @ResponseBody
    public byte[] getGallery(@PathVariable String imageName, @PathVariable String imageFlour) throws IOException {
        Path imagePath = Paths.get(uploadDir+"/"+imageFlour, imageName);
        return Files.readAllBytes(imagePath);
    }
    @GetMapping("")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "6") int pageSize,
                          Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "name"));
        if(isAdmin()){
            Page<ResidentDto> residentDtos = residentService.listAll(pageRequest);
            model.addAttribute("residents", residentDtos);
        }else{
            Page<ResidentDto> residentDtos = residentService.listAllNotDeleted(pageRequest);
            model.addAttribute("residents", residentDtos);
        }
        return "residents/listAllResidents";
    }


    @GetMapping("/{id}")
    public String getResidentInfo(@PathVariable Long id,
                                Model model) throws NotFoundException {
        ResidentDto residentDto = residentService.getOne(id);
        model.addAttribute("resident", residentDto);
        model.addAttribute("galleryFiles", getGalleryByFolder(residentDto.getGallery()));

        return "residents/viewResident";
    }

    @GetMapping("/add")
    public String create() throws AuthException {
        if(!isAdmin()){
            throw new AuthException(HttpStatus.FORBIDDEN + ": " + USER_FORBIDDEN_ERROR);
        }
        return "residents/createResident";
    }

    @PostMapping("/add")
    public String create(@RequestParam("files") MultipartFile[] files,@RequestParam("photo") MultipartFile photo, @ModelAttribute("residentForm") CreateResidentRequest residentDto) throws IOException {
        residentService.createResidentByRequest(files,photo,residentDto);
        return "redirect:/residents";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException {
        ResidentDto residentDto = residentService.getOne(id);
        model.addAttribute("resident", residentDto);
        model.addAttribute("galleryFiles", getGalleryByFolder(residentDto.getGallery()));
        return "residents/updateResident";
    }

    @PostMapping("/update")
    public String update(@RequestParam("id") Long id,@ModelAttribute("residentForm") ResidentDto residentDto, @RequestParam(value = "deletedFiles", required = false) List<String> deletedFiles,
                         @RequestParam("newFiles") MultipartFile[] newFiles, @RequestParam("photoNew") MultipartFile photo) throws IOException, NotFoundException {

        residentService.updateResidentByRequest(residentService.getOne(id).getId(), residentDto, deletedFiles, newFiles, photo);
        return "redirect:/residents";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException, NotFoundException {
        residentService.deleteSoft(id);
        return "redirect:/residents";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) throws NotFoundException {
        residentService.restore(id);
        return "redirect:/residents";
    }
    @PostMapping("/search")
    public String searchResidents(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "6") int pageSize,
                                @ModelAttribute("authorSearchForm") ResidentDto residentDto,
                                Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "name"));
        if(isAdmin()){
            model.addAttribute("residents", residentService.searchResidentsIsAdmin(residentDto.getName(), pageRequest));
        }else{
            model.addAttribute("residents", residentService.searchResidents(residentDto.getName(), pageRequest));
        }
        return "residents/listAllResidents";
    }

    @GetMapping("/report")
    public String reportResident(Model model) throws AuthException {
        if(!isAdmin()){
            throw new AuthException(HttpStatus.FORBIDDEN + ": " + USER_FORBIDDEN_ERROR);
        }
        model.addAttribute("residents",residentService.reportResident());
        return "residents/report";
    }

private List<String> getGalleryByFolder(String galleryFolder){
    String galleryPath = uploadDir + "/" + galleryFolder;
    File gallery = new File(galleryPath);
    File[] galleryFiles = gallery.listFiles();
   if (galleryFiles != null) {
       return Arrays.stream(galleryFiles).map(File::getName).toList();
   }else return new ArrayList<>();
}
    private boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                if (authority.getAuthority().equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }
}
