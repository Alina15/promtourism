package com.sez.promtourism.PromTourism.controller.rest;

import com.sez.promtourism.PromTourism.dto.TourDto;
import com.sez.promtourism.PromTourism.dto.TypeOfTourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateTourRequest;
import com.sez.promtourism.PromTourism.dto.create.CreateTypeOfTourRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.service.TypeOfTourService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/rest/typeOfTour")
@RequiredArgsConstructor
@Slf4j
public class TypeOfTourController {
    private final TypeOfTourService typeOfTourService;

    @RequestMapping(value = "/getTypeOfTour", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TypeOfTourDto> getOneTypeOfTour(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(typeOfTourService.getOne(id));
    }
}
