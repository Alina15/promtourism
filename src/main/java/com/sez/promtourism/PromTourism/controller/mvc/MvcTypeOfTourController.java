package com.sez.promtourism.PromTourism.controller.mvc;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.TypeOfTourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateResidentRequest;
import com.sez.promtourism.PromTourism.dto.create.CreateTypeOfTourRequest;
import com.sez.promtourism.PromTourism.exception.MyDeleteException;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.model.TypeOfTour;
import com.sez.promtourism.PromTourism.service.ResidentService;
import com.sez.promtourism.PromTourism.service.TypeOfTourService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
@RequestMapping("/typeOfTours")
public class MvcTypeOfTourController {
    private final TypeOfTourService typeOfTourService;
    @Value("${file.upload-dir}")
    private String uploadDir;
    public MvcTypeOfTourController(TypeOfTourService typeOfTourService) {
        this.typeOfTourService = typeOfTourService;
    }
    @GetMapping("/images/{imageName}")
    @ResponseBody
    public byte[] getImage(@PathVariable String imageName) throws IOException {
        Path imagePath = Paths.get(uploadDir+"/"+ imageName);
        return Files.readAllBytes(imagePath);
    }

    @GetMapping("")
    public String listAll(Model model) {
        if(isAdmin()){
            List<TypeOfTourDto> typeOfTourDtos = typeOfTourService.listAll();
            model.addAttribute("typeOfTours", typeOfTourDtos);
        }else {
            List<TypeOfTourDto> typeOfTourDtos = typeOfTourService.listAllNotDeleted();
            model.addAttribute("typeOfTours", typeOfTourDtos);
        }
        return "typeOfTours/listAllTypeOfTours";
    }

    @GetMapping("/add")
    public String create() {
        return "typeOfTours/createTypeOfTour";
    }

    @PostMapping("/add")
    public String create(@RequestParam("photo") MultipartFile photo, @ModelAttribute("typeOfTourForm") CreateTypeOfTourRequest typeOfTourRequest) throws IOException {
        typeOfTourService.createTourOfTourByRequest(photo,typeOfTourRequest);
        return "redirect:/typeOfTours";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException {
        TypeOfTourDto typeOfTourDto = typeOfTourService.getOne(id);
        model.addAttribute("typeOfTour", typeOfTourDto);
        return "typeOfTours/updateTypeOfTour";
    }

    @PostMapping("/update")
    public String update(@RequestParam("id") Long id,@ModelAttribute("typeOfTourForm") TypeOfTourDto typeOfTourDto, @RequestParam("photoNew") MultipartFile photo) throws IOException, NotFoundException {

        typeOfTourService.updateResidentByRequest(typeOfTourService.getOne(id).getId(), typeOfTourDto, photo);
        return "redirect:/typeOfTours";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException, NotFoundException {
        typeOfTourService.deleteSoft(id);
        return "redirect:/typeOfTours";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) throws NotFoundException {
        typeOfTourService.restore(id);
        return "redirect:/typeOfTours";
    }

    private boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                if (authority.getAuthority().equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }
}
