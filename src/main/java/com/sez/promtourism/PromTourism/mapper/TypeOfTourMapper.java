package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.TypeOfTourDto;
import com.sez.promtourism.PromTourism.model.Resident;
import com.sez.promtourism.PromTourism.model.TypeOfTour;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TypeOfTourMapper extends GenericMapperImpl<TypeOfTour, TypeOfTourDto>{
    public TypeOfTourMapper(ModelMapper modelMapper) {
        super(TypeOfTour.class, TypeOfTourDto.class, modelMapper);
    }
    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(TypeOfTour.class, TypeOfTourDto.class)
                .setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(TypeOfTourDto.class, TypeOfTour.class)
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(TypeOfTour source, TypeOfTourDto destination) {

    }

    @Override
    protected void mapSpecificFields(TypeOfTourDto source, TypeOfTour destination) {

    }

    @Override
    protected List<Long> fillIds(TypeOfTour source) {
        return null;
    }
}
