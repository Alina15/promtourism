package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.repository.RoleRepository;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import com.sez.promtourism.PromTourism.model.GenericModel;
import com.sez.promtourism.PromTourism.model.User;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapperImpl<User, UserDto>{
    private final TourRepository tourRepository;
    public UserMapper(ModelMapper modelMapper,
                      TourRepository tourRepository) {
        super(User.class, UserDto.class, modelMapper);
        this.tourRepository = tourRepository;

    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDto.class)
                .addMappings(m -> m.skip(UserDto::setTourIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDto.class, User.class)
                .addMappings(m -> m.skip(User::setTours)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {
        destination.setTourIds(fillIds(source));


    }

    @Override
    protected void mapSpecificFields(UserDto source, User destination) {
        if (!Objects.isNull(source.getTourIds())) {
            destination.setTours(tourRepository.findAllById(source.getTourIds()));
        } else {
            destination.setTours(Collections.emptyList());
        }

    }

    @Override
    protected List<Long> fillIds(User source) {
        return Objects.isNull(source) || Objects.isNull(source.getTours()) || source.getTours().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getTours().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

}
