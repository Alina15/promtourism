package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.ReviewDto;
import com.sez.promtourism.PromTourism.dto.TourDto;
import com.sez.promtourism.PromTourism.model.Review;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.model.GenericModel;
import com.sez.promtourism.PromTourism.repository.ResidentRepository;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.repository.TypeOfTourRepository;
import com.sez.promtourism.PromTourism.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Component
public class TourMapper extends GenericMapperImpl<Tour, TourDto>{
    private final ResidentRepository residentRepository;
    private final UserRepository userRepository;
    private final TypeOfTourRepository typeOfTourRepository;
    public TourMapper(ModelMapper modelMapper,
                      ResidentRepository residentRepository, UserRepository userRepository, TypeOfTourRepository typeOfTourRepository) {
        super(Tour.class, TourDto.class, modelMapper);
        this.residentRepository = residentRepository;
        this.userRepository = userRepository;
        this.typeOfTourRepository = typeOfTourRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Tour.class, TourDto.class)
                .addMappings(m -> m.skip(TourDto::setResidentId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(TourDto::setUserIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(TourDto::setTypeOfTourId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(TourDto::setStartTourFormatter)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(TourDto.class, Tour.class)
                .addMappings(m -> m.skip(Tour::setResident)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Tour::setUsers)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Tour::setTypeOfTour)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(Tour source, TourDto destination) {
        destination.setUserIds(fillIds(source));
        destination.setResidentId(getResidentId(source));
        destination.setTypeOfTourId(getTypeOfTourId(source));
        destination.setStartTourFormatter(getStartTourFormatter(source));

    }
    private Long getResidentId(Tour source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getResident().getId();
    }
    private String getStartTourFormatter(Tour source) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        return source.getStartTour().format(formatter);
    }
    private Long getTypeOfTourId(Tour source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getTypeOfTour().getId();
    }
    @Override
    protected void mapSpecificFields(TourDto source, Tour destination) {
        if (!Objects.isNull(source.getUserIds())) {
            destination.setUsers(userRepository.findAllById(source.getUserIds()));
        } else {
            destination.setUsers(Collections.emptyList());
        }
        destination.setResident(residentRepository.findById(source.getResidentId()).orElse(null));
        destination.setTypeOfTour(typeOfTourRepository.findById(source.getTypeOfTourId()).orElse(null));
    }

    @Override
    protected List<Long> fillIds(Tour source) {
        return Objects.isNull(source) || Objects.isNull(source.getUsers()) || source.getUsers().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getUsers().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
