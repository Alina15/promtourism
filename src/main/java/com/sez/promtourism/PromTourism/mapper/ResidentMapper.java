package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.model.Resident;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class ResidentMapper extends GenericMapperImpl<Resident, ResidentDto>{

    public ResidentMapper(ModelMapper modelMapper) {
        super(Resident.class, ResidentDto.class, modelMapper);
    }
    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Resident.class, ResidentDto.class)
                .setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(ResidentDto.class, Resident.class)
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(Resident source, ResidentDto destination) {

    }

    @Override
    protected void mapSpecificFields(ResidentDto source, Resident destination) {

    }

    @Override
    protected List<Long> fillIds(Resident source) {
        return null;
    }

}
