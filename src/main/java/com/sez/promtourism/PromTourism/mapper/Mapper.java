package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.GenericDto;
import com.sez.promtourism.PromTourism.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDto> {
    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDtos(List<E> entityList);
}

