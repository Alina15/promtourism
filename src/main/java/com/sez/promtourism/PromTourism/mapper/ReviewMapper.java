package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.ReviewDto;
import com.sez.promtourism.PromTourism.model.Resident;
import com.sez.promtourism.PromTourism.model.Review;
import com.sez.promtourism.PromTourism.repository.ResidentRepository;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class ReviewMapper extends GenericMapperImpl<Review, ReviewDto>{
    private final ResidentRepository residentRepository;
    private final UserRepository userRepository;
    private final TourRepository tourRepository;
    public ReviewMapper(ModelMapper modelMapper,
                        ResidentRepository residentRepository, UserRepository userRepository, TourRepository tourRepository) {
        super(Review.class, ReviewDto.class, modelMapper);
        this.residentRepository = residentRepository;
        this.userRepository = userRepository;
        this.tourRepository = tourRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Review.class, ReviewDto.class)
                .addMappings(m -> m.skip(ReviewDto::setResidentId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ReviewDto::setUserId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ReviewDto::setTourId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(ReviewDto.class, Review.class)
                .addMappings(m -> m.skip(Review::setResident)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Review::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Review::setTour)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(Review source, ReviewDto destination) {
        destination.setResidentId(getResidentId(source));
        destination.setUserId(getUserId(source));
        destination.setTourId(getTourId(source));

    }
    private Long getResidentId(Review source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getResident().getId();
    }
    private Long getUserId(Review source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getUser().getId();
    }
    private Long getTourId(Review source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getTour().getId();
    }
    @Override
    protected void mapSpecificFields(ReviewDto source, Review destination) {
        destination.setResident(residentRepository.findById(source.getResidentId()).orElse(null));
        destination.setUser(userRepository.findById(source.getUserId()).orElse(null));
        destination.setTour(tourRepository.findById(source.getTourId()).orElse(null));
    }

    @Override
    protected List<Long> fillIds(Review source) {
        return null;
    }


}
