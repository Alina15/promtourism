package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.ResidentWithReviewDto;
import com.sez.promtourism.PromTourism.dto.TourWithReviewDto;
import com.sez.promtourism.PromTourism.model.Resident;
import com.sez.promtourism.PromTourism.model.Tour;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class ResidentWithReviewMapper extends GenericMapperImpl<Resident, ResidentWithReviewDto>{
    public ResidentWithReviewMapper(ModelMapper modelMapper) {
        super(Resident.class, ResidentWithReviewDto.class, modelMapper);
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Resident.class, ResidentWithReviewDto.class)
                .setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(ResidentWithReviewDto.class, Resident.class)
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(Resident source, ResidentWithReviewDto destination) {

    }

    @Override
    protected void mapSpecificFields(ResidentWithReviewDto source, Resident destination) {

    }

    @Override
    protected List<Long> fillIds(Resident source) {
        return null;
    }
}
