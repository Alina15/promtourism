package com.sez.promtourism.PromTourism.mapper;

import com.sez.promtourism.PromTourism.dto.ReviewDto;
import com.sez.promtourism.PromTourism.dto.ReviewWithUserAndTourDto;
import com.sez.promtourism.PromTourism.model.Review;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Component
public class ReviewWithUserAndTourMapper extends GenericMapperImpl<Review, ReviewWithUserAndTourDto>{
    private final UserRepository userRepository;
    private final TourRepository tourRepository;

    public ReviewWithUserAndTourMapper(ModelMapper modelMapper,
                                       UserRepository userRepository, TourRepository tourRepository) {
        super(Review.class, ReviewWithUserAndTourDto.class, modelMapper);
        this.userRepository = userRepository;
        this.tourRepository = tourRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Review.class, ReviewWithUserAndTourDto.class)
                .addMappings(m -> m.skip(ReviewWithUserAndTourDto::setUserId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ReviewWithUserAndTourDto::setTourId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ReviewWithUserAndTourDto::setStartTourFormatter)).setPostConverter(toDtoConverter())
                .addMappings(mapper -> {
                    mapper.map(source -> source.getUser().getFirstName(), ReviewWithUserAndTourDto::setFirstName);
                    mapper.map(source -> source.getUser().getLastName(), ReviewWithUserAndTourDto::setLastName);
                    mapper.map(source -> source.getTour().getStartTour(), ReviewWithUserAndTourDto::setStartTour);
                    mapper.map(source -> source.getTour().getTypeOfTour().getName(), ReviewWithUserAndTourDto::setNameTypeOfTour);
                });


        modelMapper.createTypeMap(ReviewWithUserAndTourDto.class, Review.class)
                .addMappings(m -> m.skip(Review::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Review::setTour)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(Review source, ReviewWithUserAndTourDto destination) {
        destination.setUserId(getUserId(source));
        destination.setTourId(getTourId(source));
        destination.setStartTourFormatter(getStartTourFormatter(source.getTour()));
    }

    @Override
    protected void mapSpecificFields(ReviewWithUserAndTourDto source, Review destination) {
        destination.setUser(userRepository.findById(source.getUserId()).orElse(null));
        destination.setTour(tourRepository.findById(source.getTourId()).orElse(null));
    }
    private Long getUserId(Review source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getUser().getId();
    }
    private Long getTourId(Review source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getTour().getId();
    }
    private String getStartTourFormatter(Tour source) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        return source.getStartTour().format(formatter);
    }
    @Override
    protected List<Long> fillIds(Review source) {
        return null;
    }
}


