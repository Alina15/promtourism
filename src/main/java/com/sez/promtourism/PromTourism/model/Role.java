package com.sez.promtourism.PromTourism.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Entity
@Table(name = "role", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;

    @ManyToMany(cascade =  CascadeType.MERGE)
    @JoinTable(name = "users_role",
            joinColumns = @JoinColumn(name = "role_id"), foreignKey = @ForeignKey(name = "FK_ROLE_USERS"),
            inverseJoinColumns = @JoinColumn(name = "user_id"), inverseForeignKey = @ForeignKey(name = "FK_USERS_ROLE")
    )
    private List<User> users;
    @Override
    public String getAuthority() {
        return "ROLE_" + this.name;
    }

}
