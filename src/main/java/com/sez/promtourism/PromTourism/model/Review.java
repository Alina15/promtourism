package com.sez.promtourism.PromTourism.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "reviews", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Review extends GenericModel{
    @Column(nullable = false)
    private Integer score;
    private String description;
    @ManyToOne(cascade =  CascadeType.MERGE)
    @JoinColumn(name = "users_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_USER_REVIEW"))
    private User user;
    @ManyToOne(cascade =  CascadeType.MERGE)
    @JoinColumn(name = "residents_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_RESIDENT_REVIEW"))
    private Resident resident;
    @ManyToOne(cascade =  CascadeType.MERGE)
    @JoinColumn(name = "tour_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_TOUR_REVIEW"))
    private Tour tour;

}
