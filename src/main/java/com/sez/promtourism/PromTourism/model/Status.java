package com.sez.promtourism.PromTourism.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public enum Status {
    ASSIGNED("Запланирован"),
    COMPLETED("Проведён"),
    CANCELLED("Отменён");

    private final String statusTextDisplay;

    Status(String text) {
        this.statusTextDisplay = text;
    }
}
