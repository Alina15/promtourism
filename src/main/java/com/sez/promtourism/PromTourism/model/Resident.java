package com.sez.promtourism.PromTourism.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "residents", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Resident extends GenericModel{
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private String photo;
    private String gallery;
}
