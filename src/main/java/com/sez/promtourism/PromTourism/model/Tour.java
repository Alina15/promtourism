package com.sez.promtourism.PromTourism.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "tour", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tour extends GenericModel{
    @Column(nullable = false)
    private String name;
    private Integer numberOfPeople;
    private Integer ageAt;
    private LocalDateTime startTour;
    private Integer numberOfRegisteredPeople = 0;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "resident_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_RESIDENT_TOUR"))
    private Resident resident;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "type_of_tour_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_TYPE_OF_TOUR_TOUR"))
    private TypeOfTour typeOfTour;
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private Status status;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "users_tour",
            joinColumns = @JoinColumn(name = "tour_id"), foreignKey = @ForeignKey(name = "FK_TOUR_USERS"),
            inverseJoinColumns = @JoinColumn(name = "user_id"), inverseForeignKey = @ForeignKey(name = "FK_USERS_TOUR")
    )
    private List<User> users;

}
