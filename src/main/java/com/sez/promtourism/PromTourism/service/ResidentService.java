package com.sez.promtourism.PromTourism.service;

import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.ResidentWithReviewDto;
import com.sez.promtourism.PromTourism.dto.create.CreateResidentRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.mapper.ResidentMapper;
import com.sez.promtourism.PromTourism.mapper.ResidentWithReviewMapper;
import com.sez.promtourism.PromTourism.model.Resident;
import com.sez.promtourism.PromTourism.model.Review;
import com.sez.promtourism.PromTourism.repository.ResidentRepository;
import com.sez.promtourism.PromTourism.repository.ReviewRepository;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class ResidentService extends GenericService<Resident, ResidentDto>{
    @Value("${file.upload-dir}")
    private String uploadDir;
    private final ResidentWithReviewMapper residentWithReviewMapper;
    private final TourRepository tourRepository;
    private final ReviewRepository reviewRepository;

    public ResidentService(ResidentRepository repository,
                           ResidentMapper mapper, ResidentWithReviewMapper residentWithReviewMapper, TourRepository tourRepository,ReviewRepository reviewRepository) {
        super(repository, mapper);
        this.residentWithReviewMapper = residentWithReviewMapper;
        this.tourRepository = tourRepository;
        this.reviewRepository = reviewRepository;

    }
    public void createResidentByRequest(MultipartFile[] files, MultipartFile file, final CreateResidentRequest createResidentRequest) throws IOException {
        ResidentDto residentDto = new ResidentDto();
        if(file != null) {
            residentDto.setPhoto(super.addPhoto(file));
        }
            String gallery = StringUtils.cleanPath(UUID.randomUUID().toString());
            super.addGallery(files, gallery);
            residentDto.setGallery(gallery);

        residentDto.setName(createResidentRequest.getName());
        residentDto.setDescription(createResidentRequest.getDescription());

        super.create(residentDto);
    }
    public void updateResidentByRequest(Long id, ResidentDto residentDto, List<String> deletedFiles,
                                        MultipartFile[] newFiles, MultipartFile photo) throws IOException, NotFoundException {
        ResidentDto residentDtoOld = super.getOne(id);
            super.deleteGallery(deletedFiles, residentDtoOld.getGallery());
        if(residentDtoOld.getGallery() == null){
            String gallery = StringUtils.cleanPath(UUID.randomUUID().toString());
            super.addGallery(newFiles, gallery);
            residentDto.setGallery(gallery);
        }else {
            super.addGallery(newFiles, residentDtoOld.getGallery());
            residentDto.setGallery(residentDtoOld.getGallery());
        }
        if(photo != null && !photo.isEmpty()){
            residentDto.setPhoto(super.addPhoto(photo));
        }else {
            residentDto.setPhoto(residentDtoOld.getPhoto());
        }
        super.update(residentDto, id);
    }
//    public List<ReviewDto> getReviewsByResidentId(final Long residentId) throws NotFoundException {
//        List<Review> reviews = reviewService.repository.findAllById(Collections.singleton(residentId));
//        return reviewService.mapper.toDtos(reviews);
//    }
public Page<ResidentDto> searchResidents(final String name,
                                       final Pageable pageable) {
    Page<Resident> foundResidents = ((ResidentRepository) repository).findAllByNameContainsIgnoreCaseAndIsDeletedFalse(name, pageable);
    List<ResidentDto> result = mapper.toDtos(foundResidents.getContent());
    return new PageImpl<>(result, pageable, foundResidents.getTotalElements());
}
    public Page<ResidentDto> searchResidentsIsAdmin(final String name,
                                             final Pageable pageable) {
        Page<Resident> foundResidents = ((ResidentRepository) repository).findAllByNameContainsIgnoreCase(name, pageable);
        List<ResidentDto> result = mapper.toDtos(foundResidents.getContent());
        return new PageImpl<>(result, pageable, foundResidents.getTotalElements());
    }
    public List<ResidentWithReviewDto> reportResident(){
        List<Resident> resident = repository.findAll();
        List<ResidentWithReviewDto> residentWithReviewDtos = residentWithReviewMapper.toDtos(resident);
        for(ResidentWithReviewDto residentWithReviewDto: residentWithReviewDtos){
            residentWithReviewDto.setNumberOfTours(tourRepository.findAllByResidentId(residentWithReviewDto.getId()).size());
            List<Review> reviews = reviewRepository.findAllByResidentId(residentWithReviewDto.getId());
            residentWithReviewDto.setNumberOfReviews(reviews.size());
            double sum = 0;
            for(Review review: reviews){
                sum = sum + review.getScore();
            }
            residentWithReviewDto.setAverageRating(sum/reviews.size());
        }
        return residentWithReviewDtos;
    }

}
