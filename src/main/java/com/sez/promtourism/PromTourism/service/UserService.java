package com.sez.promtourism.PromTourism.service;

import com.sez.promtourism.PromTourism.constants.MailConstants;
import com.sez.promtourism.PromTourism.dto.RoleDto;
import com.sez.promtourism.PromTourism.dto.TourWithReviewDto;
import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.mapper.TourWithReviewMapper;
import com.sez.promtourism.PromTourism.mapper.UserMapper;
import com.sez.promtourism.PromTourism.model.Status;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.model.User;
import com.sez.promtourism.PromTourism.repository.ReviewRepository;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

import static com.sez.promtourism.PromTourism.model.Status.COMPLETED;

@Service
public class UserService extends GenericService<User, UserDto> {
    @Value("${spring.mail.username}")
    private String mailServerUsername;
    private final BCryptPasswordEncoder passwordEncoder;
    private final TourWithReviewMapper tourWithReviewMapper;
    private final TourRepository tourRepository;
    private final ReviewRepository reviewRepository;
    private final JavaMailSender javaMailSender;

    public UserService(UserRepository repository,
                       UserMapper mapper,
                       BCryptPasswordEncoder passwordEncoder, TourRepository tourRepository,ReviewRepository reviewRepository, TourWithReviewMapper tourWithReviewMapper,
                       JavaMailSender javaMailSender) {
        super(repository, mapper);
        this.passwordEncoder = passwordEncoder;
        this.tourRepository = tourRepository;
        this.reviewRepository = reviewRepository;
        this.tourWithReviewMapper=tourWithReviewMapper;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public UserDto create(UserDto userDto) {
        RoleDto roleDto = new RoleDto();
        if (isAdmin()) {
            roleDto.setId(1L);
        } else {
            roleDto.setId(2L);
        }
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userDto.setRoles(List.of(roleDto));
        userDto.setCreatedWhen(LocalDateTime.now());
        userDto.setCreatedBy("");
        return mapper.toDto(repository.save(mapper.toEntity(userDto)));
    }

    public UserDto update(UserDto userDtoNew, Long id) throws NotFoundException {
        UserDto userDto = super.getOne(id);
        userDto.setMiddleName(userDtoNew.getMiddleName());
        userDto.setLastName(userDtoNew.getLastName());
        userDto.setFirstName(userDtoNew.getFirstName());
        userDto.setBirthDate(userDtoNew.getBirthDate());
        userDto.setAddress(userDtoNew.getAddress());
        userDto.setUpdatedBy(userDto.getLogin());
        userDto.setUpdatedWhen(LocalDateTime.now());
        userDto.setChangePasswordToken(userDtoNew.getChangePasswordToken());
        return mapper.toDto(repository.save(mapper.toEntity(userDto)));
    }

//    public boolean checkPassword(final String passwordToCheck,
//                                 final UserDetails userDetails) {
//        return passwordEncoder.matches(passwordToCheck, userDetails.getPassword());
//    }
    public void sendChangePasswordEmail(final UserDto userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        mapper.toDto(repository.save(mapper.toEntity(userDTO)));
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(mailServerUsername);
        mailMessage.setTo(userDTO.getEmail());
        mailMessage.setSubject(MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD);
        mailMessage.setText(MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + uuid);
        javaMailSender.send(mailMessage);
    }
    public void changePassword(final String uuid,
                               final String password) {
        UserDto userDTO = mapper.toDto(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        userDTO.setChangePasswordToken(null);
        userDTO.setPassword(passwordEncoder.encode(password));
        mapper.toDto(repository.save(mapper.toEntity(userDTO)));
    }

    public UserDto getUserByLogin(final String login) {
        return mapper.toDto(((UserRepository) repository).findUserByLoginAndDeletedFalse(login));
    }

    public UserDto getUserByEmail(final String email) {
        return mapper.toDto(((UserRepository) repository).findUserByEmailAndDeletedFalse(email));
    }

    public UserDto getUserByPhone(final String phone) {
        return mapper.toDto(((UserRepository) repository).findUserByPhoneAndDeletedFalse(phone));
    }
    private boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                if (authority.getAuthority().equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }
    public Page<TourWithReviewDto> getTourByUserId(final Long userId, PageRequest pageRequest) throws NotFoundException {
        User user = super.repository.getReferenceById(userId);
        Page<Tour> toursPage = tourRepository.findAllByUsers(user, pageRequest);
        List<TourWithReviewDto> tourDtos = new ArrayList<>();
        for (Tour tour : toursPage.getContent()) {
            TourWithReviewDto tourDto = tourWithReviewMapper.toDto(tour);
            if (!reviewRepository.existsByUserIdAndTourId(userId, tourDto.getId()) && tourDto.getStatus() == COMPLETED) {
                tourDto.setIsReview(true);
            } else if (reviewRepository.existsByUserIdAndTourId(userId, tourDto.getId())) {
                tourDto.setIsReview(false);
            }
            tourDtos.add(tourDto);
        }
        return new PageImpl<>(tourDtos, toursPage.getPageable(), toursPage.getTotalElements());
    }
    public Page<TourWithReviewDto> getAll(PageRequest pageRequest) {
        Page<Tour> tours = tourRepository.findAllByIsDeletedFalse(pageRequest);
        List<TourWithReviewDto> result = tourWithReviewMapper.toDtos(tours.getContent());
        return new PageImpl<>(result, pageRequest, tours.getTotalElements());
    }
    public Page<TourWithReviewDto> searchTour(final Status status,
                                    final Pageable pageable) {
        Page<Tour> tours = tourRepository.findAllByStatus(status, pageable);
        List<TourWithReviewDto> result = tourWithReviewMapper.toDtos(tours.getContent());
        return new PageImpl<>(result, pageable, tours.getTotalElements());
    }
}
