package com.sez.promtourism.PromTourism.service;


import com.sez.promtourism.PromTourism.dto.TypeOfTourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateTypeOfTourRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.mapper.TypeOfTourMapper;
import com.sez.promtourism.PromTourism.model.TypeOfTour;
import com.sez.promtourism.PromTourism.repository.TypeOfTourRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class TypeOfTourService extends GenericService<TypeOfTour, TypeOfTourDto>{
    @Value("${file.upload-dir}")
    private String uploadDir;
    public TypeOfTourService(TypeOfTourRepository repository,
                             TypeOfTourMapper mapper) {
        super(repository, mapper);
    }
    public void createTourOfTourByRequest(MultipartFile file, final CreateTypeOfTourRequest createTypeOfTourRequest) throws IOException {
        TypeOfTourDto typeOfTourDto = new TypeOfTourDto();
        if(file != null) {
            typeOfTourDto.setPhoto(super.addPhoto(file));
        }
        typeOfTourDto.setName(createTypeOfTourRequest.getName());
        typeOfTourDto.setDuration(createTypeOfTourRequest.getDuration());
        typeOfTourDto.setPrice(createTypeOfTourRequest.getPrice());
        typeOfTourDto.setProgram(createTypeOfTourRequest.getProgram());
        super.create(typeOfTourDto);
    }

    public void updateResidentByRequest(Long id, TypeOfTourDto typeOfTourDto, MultipartFile photo) throws IOException, NotFoundException {
        TypeOfTourDto typeOfTourDtoOld = super.getOne(id);
        if(photo != null && !photo.isEmpty()){
            typeOfTourDto.setPhoto(super.addPhoto(photo));
        }else {
            typeOfTourDto.setPhoto(typeOfTourDtoOld.getPhoto());
        }
        super.update(typeOfTourDto, id);
    }


}
