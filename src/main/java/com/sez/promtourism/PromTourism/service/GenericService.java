package com.sez.promtourism.PromTourism.service;

import com.sez.promtourism.PromTourism.dto.GenericDto;
import com.sez.promtourism.PromTourism.exception.MyDeleteException;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.mapper.GenericMapperImpl;
import com.sez.promtourism.PromTourism.model.GenericModel;
import com.sez.promtourism.PromTourism.repository.GenericRepository;
import com.sez.promtourism.PromTourism.service.userdetails.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static com.sez.promtourism.PromTourism.constants.Error.REST.DELETE_ERROR_MESSAGE;

@Service
@Slf4j
public abstract class GenericService<T extends GenericModel, N extends GenericDto> {
    protected final GenericRepository<T> repository;
    protected final GenericMapperImpl<T, N> mapper;
    @Value("${file.upload-dir}")
    private String uploadDir;

    public GenericService(GenericRepository<T> repository,
                          GenericMapperImpl<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDtos(repository.findAll());
    }

    public List<N> listAllNotDeleted() {
        return mapper.toDtos(repository.findAllByIsDeletedFalse());
    }

    public Page<N> listAll(Pageable pageable) {
        Page<T> preResult = repository.findAll(pageable);
        List<N> result = mapper.toDtos(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }

    public Page<N> listAllNotDeleted(Pageable pageable) {
        Page<T> preResult = repository.findAllByIsDeletedFalse(pageable);
        List<N> result = mapper.toDtos(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }

    public N getOne(final Long id) throws NotFoundException {
        return mapper.toDto(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных не найдено по заданному id")));
    }

    public N create(N newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        newEntity.setCreatedBy(customUserDetails.getUsername());
        T entity = mapper.toEntity(newEntity);
        return mapper.toDto(repository.save(entity));
    }

    public N update(N updatedEntity, final Long id) throws NotFoundException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        updatedEntity.setUpdatedBy(customUserDetails.getUsername());
        updatedEntity.setUpdatedWhen(LocalDateTime.now());
        updatedEntity.setId(id);
        return mapper.toDto(repository.save(mapper.toEntity(updatedEntity)));
    }

    public void delete(final Long id) {
        repository.deleteById(id);
    }

    public void deleteSoft(final Long id) throws MyDeleteException, NotFoundException {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден"));
        markAsDeleted(obj);
        repository.save(obj);
    }

    public void restore(final Long id) throws NotFoundException {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден"));
        unMarkAsDeleted(obj);
        repository.save(obj);
    }

    private void markAsDeleted(T obj) {
        obj.setDeleted(true);
        obj.setDeletedWhen(LocalDateTime.now());
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        obj.setDeletedBy(customUserDetails.getUsername());
    }

    private void unMarkAsDeleted(T obj) {
        obj.setDeleted(false);
        obj.setDeletedWhen(null);
        obj.setDeletedBy(null);
    }
    public String addPhoto(MultipartFile photo) throws IOException {
            String fileName = StringUtils.cleanPath(UUID.randomUUID().toString() + "_" + photo.getOriginalFilename());
            Path uploadPath = Paths.get(uploadDir);
            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(photo.getInputStream(), filePath);
        return fileName;
    }
    public void addGallery(MultipartFile[] files, String gallery) throws IOException {
        if(files != null && !Objects.equals(files[0].getOriginalFilename(), "")) {

            String galleryPathToString = uploadDir + "/" + gallery;
            Path galleryPath = Paths.get(galleryPathToString);
            if (!Files.exists(galleryPath)) {
                Files.createDirectories(galleryPath);
            }
            for (MultipartFile fileGal : files) {
                String fileNameGal = StringUtils.cleanPath(UUID.randomUUID().toString() + "_" + fileGal.getOriginalFilename());
                Path filePathGal = galleryPath.resolve(fileNameGal);
                Files.copy(fileGal.getInputStream(), filePathGal);
            }
        }
    }

    public void deleteGallery(List<String> deletedFiles, String gallery){
        if (deletedFiles != null && !deletedFiles.isEmpty()) {
            for (String fileName : deletedFiles) {
                File file = new File(uploadDir + "/" + gallery + "/" + fileName);
                boolean deleted = file.delete();
                if (!deleted) {
                    System.err.println(DELETE_ERROR_MESSAGE + fileName);
                }
            }
        }
    }
}


