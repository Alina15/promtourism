package com.sez.promtourism.PromTourism.service;

import com.sez.promtourism.PromTourism.dto.*;
import com.sez.promtourism.PromTourism.dto.create.CreateTourRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.mapper.TourMapper;
import com.sez.promtourism.PromTourism.model.Resident;
import com.sez.promtourism.PromTourism.model.Status;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.repository.ResidentRepository;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.sez.promtourism.PromTourism.model.Status.ASSIGNED;

@Service
public class TourService extends GenericService<Tour, TourDto>{
    private final UserService userService;
    private final TourRepository repository;
    public TourService(TourRepository repository,
                       TourMapper mapper, UserService userService) {
        super(repository, mapper);
        this.userService=userService;
        this.repository=repository;
    }
    public void createTourByRequest(final CreateTourRequest createTourRequest) {
        TourDto tourDto = new TourDto();
        tourDto.setName(createTourRequest.getName());
        tourDto.setNumberOfPeople(createTourRequest.getNumberOfPeople());
        tourDto.setAgeAt(createTourRequest.getAgeAt());
        tourDto.setResidentId(createTourRequest.getResidentId());
        tourDto.setStartTour(createTourRequest.getStartTour());
        tourDto.setTypeOfTourId(createTourRequest.getTypeOfTourId());
        tourDto.setStatus(ASSIGNED);
        tourDto.setUserIds(createTourRequest.getUserIds());
        tourDto.setNumberOfRegisteredPeople(0);
        super.create(tourDto);
    }
    public List<UserDto> getUsersByTourId(final Long tourId) throws NotFoundException {
        TourDto tourDto = getOne(tourId);
        List<UserDto> userDtos = new ArrayList<>();
        for (Long id : tourDto.getUserIds()) {
            userDtos.add(userService.getOne(id));
        }
        return userDtos;
    }
    public void updateTour(TourDto tourDtoNew, Long id) throws NotFoundException{
        TourDto tourDto = getOne(id);
        tourDtoNew.setUserIds(tourDto.getUserIds());
        super.update(tourDtoNew, id);
    }
    public void addTour(final AddOrDeleteTourDto addOrDeleteTourDto) throws NotFoundException {
        TourDto tourDto = getOne(addOrDeleteTourDto.getTourId());
        if (tourDto.getUserIds().isEmpty()){
            tourDto.setUserIds(Collections.singletonList(addOrDeleteTourDto.getUserId()));
        }else {
            tourDto.getUserIds().add(addOrDeleteTourDto.getUserId());
        }
        tourDto.setNumberOfRegisteredPeople(tourDto.getNumberOfRegisteredPeople()+1);
        update(tourDto, addOrDeleteTourDto.getTourId());
    }
    public void deleteTourIdByUserId (final AddOrDeleteTourDto addOrDeleteTourDto) throws NotFoundException {
        TourDto tourDto = getOne(addOrDeleteTourDto.getTourId());
            tourDto.getUserIds().remove(addOrDeleteTourDto.getUserId());
        tourDto.setNumberOfRegisteredPeople(tourDto.getNumberOfRegisteredPeople()-1);
        update(tourDto, addOrDeleteTourDto.getTourId());
    }
    public List<TourDto> findAllByResidentId(Long residentId){
        List<Tour> tours = repository.findAllByResidentIdAndStatus(residentId, ASSIGNED);
        return mapper.toDtos(tours);
    }

}
