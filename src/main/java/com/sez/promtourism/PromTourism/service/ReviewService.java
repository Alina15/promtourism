package com.sez.promtourism.PromTourism.service;

import com.sez.promtourism.PromTourism.dto.ReviewDto;
import com.sez.promtourism.PromTourism.dto.ReviewWithUserAndTourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateReviewRequest;
import com.sez.promtourism.PromTourism.exception.NotFoundException;
import com.sez.promtourism.PromTourism.mapper.ReviewMapper;
import com.sez.promtourism.PromTourism.mapper.ReviewWithUserAndTourMapper;
import com.sez.promtourism.PromTourism.model.Review;
import com.sez.promtourism.PromTourism.repository.ReviewRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewService extends GenericService<Review, ReviewDto>{
    private final ReviewWithUserAndTourMapper reviewWithUserAndTourMapper;

    public ReviewService(ReviewRepository repository,
                         ReviewMapper mapper, ReviewWithUserAndTourMapper reviewWithUserAndTourMapper) {
        super(repository, mapper);
        this.reviewWithUserAndTourMapper = reviewWithUserAndTourMapper;
    }
    public void createReviewByRequest(final CreateReviewRequest createReviewRequest) {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setScore(createReviewRequest.getScore());
        reviewDto.setDescription(createReviewRequest.getDescription());
        reviewDto.setTourId(createReviewRequest.getTourId());
        reviewDto.setResidentId(createReviewRequest.getResidentId());
        reviewDto.setUserId(createReviewRequest.getUserId());
        super.create(reviewDto);
    }
    public Page<ReviewWithUserAndTourDto> getAllNotDeletedReviewWithUserAndTour(PageRequest pageRequest) {
        Page<Review> reviews = repository.findAllByIsDeletedFalse(pageRequest);
        List<ReviewWithUserAndTourDto> result = reviewWithUserAndTourMapper.toDtos(reviews.getContent());
        return new PageImpl<>(result, pageRequest, reviews.getTotalElements());
    }
public boolean getExistsByUserIdAndTourId(Long userId, Long tourId){
        return ((ReviewRepository)repository).existsByUserIdAndTourId(userId,tourId);
}

}
