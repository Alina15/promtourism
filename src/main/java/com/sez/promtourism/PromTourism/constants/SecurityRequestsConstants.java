package com.sez.promtourism.PromTourism.constants;

import java.util.List;

public interface SecurityRequestsConstants {
    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/static/**",
            "/js/**",
            "/css/**",
            "/img/**",
            "/fonts/**",
            "/",
            "/error",
            "/swagger-ui/**",
            "/rest/**",
            "/user-controller/**",
            "/policy/**",
            "/webjars/bootstrap/5.0.2/**",
            "/v3/api-docs/**",
            "/tours/search");

    List<String> RESIDENTS_WHITE_LIST = List.of("/residents",
            "/residents/search",
            "/residents/{id}",
            "/residents/images/**",
            "/residents/gallery/**");

    List<String> TYPE_OF_TOUR_WHITE_LIST = List.of("/typeOfTours",
            "/typeOfTours/images/**");
    List<String> REVIEWS_WHITE_LIST = List.of("/reviews",
//            "/reviews/{id}",
            "/reviews/add",
            "/reviews/update",
            "/reviews/delete");
    List<String> ABOUT_WHITE_LIST = List.of("/about");

    List<String> RESIDENTS_PERMISSION_LIST = List.of("/residents/add",
            "/residents/update",
            "/residents/delete",
            "/residents/report");

    List<String> TYPE_OF_TOUR_PERMISSION_LIST = List.of("/typeOfTours/add",
            "/typeOfTours/update",
            "/typeOfTours/delete");

    List<String> USERS_WHITE_LIST = List.of("/login",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password",
            "/users/auth");

    List<String> USERS_PERMISSION_LIST = List.of("/tours/addUserTour");
}
