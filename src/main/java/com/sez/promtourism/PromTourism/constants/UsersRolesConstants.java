package com.sez.promtourism.PromTourism.constants;

public interface UsersRolesConstants {
    String USER = "USER";
    String ADMIN = "ADMIN";
}

