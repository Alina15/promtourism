package com.sez.promtourism.PromTourism;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromTourismApplication {

	public static void main(String[] args) {
		SpringApplication.run(PromTourismApplication.class, args);
	}

}
