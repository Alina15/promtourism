package com.sez.promtourism.PromTourism.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends GenericDto {
    private String login;
    private String password;
    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private List<RoleDto> roles = new ArrayList<>();
    private List<Long> tourIds = new ArrayList<>();
    private String changePasswordToken;
}
