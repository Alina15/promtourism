package com.sez.promtourism.PromTourism.dto.create;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateReviewRequest {
    private Integer score;
    private String description;
    private Long userId;
    private Long residentId;
    private Long tourId;
}
