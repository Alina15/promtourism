package com.sez.promtourism.PromTourism.dto;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TourWithReviewDto extends TourDto{
    private Boolean isReview;
    private String nameTypeOfTour;
    private String nameResident;

}
