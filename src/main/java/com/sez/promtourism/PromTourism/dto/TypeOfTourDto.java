package com.sez.promtourism.PromTourism.dto;

import jakarta.persistence.Column;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TypeOfTourDto extends GenericDto {
    private String name;
    private Double duration;
    private Integer price;
    private String program;
    private String photo;
}
