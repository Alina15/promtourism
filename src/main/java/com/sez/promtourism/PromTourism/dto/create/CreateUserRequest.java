package com.sez.promtourism.PromTourism.dto.create;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Data
public class CreateUserRequest {
    private String login;
    private String password;
    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private List<Long> roleIds = new ArrayList<>();
    private List<Long> tourIds = new ArrayList<>();
    private String changePasswordToken;
}
