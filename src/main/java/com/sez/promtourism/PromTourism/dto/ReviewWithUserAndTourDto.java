package com.sez.promtourism.PromTourism.dto;

import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.model.User;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Set;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReviewWithUserAndTourDto extends ReviewDto {
    private LocalDateTime startTour;
    private String startTourFormatter;
    private String firstName;
    private String lastName;
    private String nameTypeOfTour;
}
