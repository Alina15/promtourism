package com.sez.promtourism.PromTourism.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto extends GenericDto{
    //private Long id;
    private String name;
    private String description;
    private List<Long> userIds = new ArrayList<>();
}
