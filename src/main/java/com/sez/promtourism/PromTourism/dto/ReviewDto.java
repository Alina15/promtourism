package com.sez.promtourism.PromTourism.dto;


import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReviewDto extends GenericDto {
    private Integer score;
    private String description;
    private Long userId;
    private Long residentId;
    private Long tourId;
}
