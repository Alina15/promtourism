package com.sez.promtourism.PromTourism.dto;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResidentDto  extends GenericDto {
    private String name;
    private String description;
    private String photo;
    private String gallery;
}
