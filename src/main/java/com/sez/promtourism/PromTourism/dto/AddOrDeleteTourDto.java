package com.sez.promtourism.PromTourism.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddOrDeleteTourDto {
    private Long userId;
    private Long tourId;
}
