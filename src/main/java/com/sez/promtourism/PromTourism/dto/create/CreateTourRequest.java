package com.sez.promtourism.PromTourism.dto.create;

import com.sez.promtourism.PromTourism.model.Status;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateTourRequest {
    private String name;
    private Integer numberOfPeople;
    private Integer ageAt;
    private LocalDateTime startTour;
    private Integer numberOfRegisteredPeople;
    private Long residentId;
    private Long typeOfTourId;
    private Status status;
    private List<Long> userIds = new ArrayList<>();
}
