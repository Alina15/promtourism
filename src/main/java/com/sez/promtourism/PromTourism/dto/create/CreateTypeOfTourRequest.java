package com.sez.promtourism.PromTourism.dto.create;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateTypeOfTourRequest {
    private String name;
    private Double duration;
    private Integer price;
    private String program;

}
