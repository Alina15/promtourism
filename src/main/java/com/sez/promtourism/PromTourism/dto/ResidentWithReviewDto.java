package com.sez.promtourism.PromTourism.dto;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResidentWithReviewDto extends ResidentDto{
    private Double averageRating = 0.0;
    private Integer numberOfTours = 0;
    private Integer numberOfReviews = 0;
}
