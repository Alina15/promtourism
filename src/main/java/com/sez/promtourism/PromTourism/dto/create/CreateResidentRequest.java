package com.sez.promtourism.PromTourism.dto.create;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateResidentRequest {
    private String name;
    private String description;
}
