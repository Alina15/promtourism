package com.sez.promtourism.PromTourism.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GenericDto {
    protected Long id;
    protected String createdBy;
    protected LocalDateTime createdWhen;
    protected String updatedBy;
    protected LocalDateTime updatedWhen;
    protected LocalDateTime deletedWhen;
    protected String deletedBy;
    protected boolean isDeleted;

}

