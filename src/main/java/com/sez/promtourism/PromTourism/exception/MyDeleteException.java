package com.sez.promtourism.PromTourism.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(final String message) {
        super(message);
    }
}

