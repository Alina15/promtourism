package com.sez.promtourism.PromTourism.exception;

public class NotFoundException extends Exception {
    public NotFoundException(final String message) {
        super(message);
    }
}
