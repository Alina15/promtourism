package com.sez.promtourism.PromTourism.repository;

import com.sez.promtourism.PromTourism.model.Status;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TourRepository extends GenericRepository<Tour>{
    List<Tour> findAllByResidentIdAndStatus(Long residentId, Status status);
    List<Tour> findAllByResidentId(Long residentId);
    Page<Tour> findAllByStatus(Status status, Pageable pageable);

    Page<Tour> findAllByUsers(User users, PageRequest pageRequest);
    Tour findByName(String name);
}
