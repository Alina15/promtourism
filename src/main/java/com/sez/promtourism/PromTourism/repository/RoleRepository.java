package com.sez.promtourism.PromTourism.repository;

import com.sez.promtourism.PromTourism.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
