package com.sez.promtourism.PromTourism.repository;

import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User>{
    @Query(nativeQuery = true,
            value = """
                select *
                from users
                where upper(login) = upper(:login)
                and is_deleted = false
            """)
    User findUserByLoginAndDeletedFalse(final String login);

    @Query(nativeQuery = true,
            value = """
                        select *
                        from users
                        where upper(email) = upper(:email)
                        and is_deleted = false
                    """)
    User findUserByEmailAndDeletedFalse(final String email);

    @Query(nativeQuery = true,
            value = """
                        select *
                        from users
                        where upper(phone) = upper(:phone)
                        and is_deleted = false
                    """)
    User findUserByPhoneAndDeletedFalse(final String phone);
    User findUserByChangePasswordToken(String uuid);

}
