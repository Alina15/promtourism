package com.sez.promtourism.PromTourism.repository;

import com.sez.promtourism.PromTourism.model.Resident;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface ResidentRepository extends GenericRepository<Resident> {
    Page<Resident> findAllByNameContainsIgnoreCaseAndIsDeletedFalse(String fio, Pageable pageable);
    Page<Resident> findAllByNameContainsIgnoreCase(String fio, Pageable pageable);
}
