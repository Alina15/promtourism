package com.sez.promtourism.PromTourism.repository;

import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.model.TypeOfTour;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeOfTourRepository extends GenericRepository<TypeOfTour>{
    TypeOfTour findByName(String name);
}
