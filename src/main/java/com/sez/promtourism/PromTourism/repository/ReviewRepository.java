package com.sez.promtourism.PromTourism.repository;

import com.sez.promtourism.PromTourism.model.Review;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends GenericRepository<Review> {
    boolean existsByUserIdAndTourId(Long UserId, Long TourId);
    List<Review> findAllByResidentId(Long id);
    Review findByDescription(String description);
}
