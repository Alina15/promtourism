CREATE TABLE public.role (
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL
);
CREATE TABLE public.users (
    id BIGSERIAL PRIMARY KEY,
    login VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    first_name VARCHAR(255) NOT NULL,
    middle_name VARCHAR(255),
    last_name VARCHAR(255) NOT NULL,
    birth_date DATE NOT NULL,
    phone VARCHAR(255) UNIQUE,
    address VARCHAR(255),
    created_when TIMESTAMP,
    created_by VARCHAR(255),
    updated_when TIMESTAMP,
    updated_by VARCHAR(255),
    is_deleted BOOLEAN DEFAULT FALSE,
    deleted_when TIMESTAMP,
    deleted_by VARCHAR(255)
);
CREATE TABLE public.users_role (
    role_id BIGINT NOT NULL,
    user_id BIGINT NOT NULL,
    CONSTRAINT FK_ROLE_USERS FOREIGN KEY (role_id) REFERENCES public.role (id) ON DELETE CASCADE,
    CONSTRAINT FK_USERS_ROLE FOREIGN KEY (user_id) REFERENCES public.users (id) ON DELETE CASCADE
);

CREATE TABLE public.type_of_tour (
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    duration DECIMAL NOT NULL,
    price INTEGER NOT NULL,
    photo VARCHAR(255) NOT NULL,
    program TEXT,
    created_when TIMESTAMP,
    created_by VARCHAR(255),
    updated_when TIMESTAMP,
    updated_by VARCHAR(255),
    is_deleted BOOLEAN DEFAULT FALSE,
    deleted_when TIMESTAMP,
    deleted_by VARCHAR(255)
);

CREATE TABLE public.residents (
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    photo VARCHAR(255) NOT NULL,
    gallery VARCHAR(255),
    created_when TIMESTAMP,
    created_by VARCHAR(255),
    updated_when TIMESTAMP,
    updated_by VARCHAR(255),
    is_deleted BOOLEAN DEFAULT FALSE,
    deleted_when TIMESTAMP,
    deleted_by VARCHAR(255)
);
CREATE TABLE public.tour (
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    number_of_people INTEGER,
    age_at INTEGER,
    start_tour TIMESTAMP,
    resident_id BIGINT NOT NULL,
    type_of_tour_id BIGINT NOT NULL,
    status smallint NOT NULL,
    created_when TIMESTAMP,
    created_by VARCHAR(255),
    updated_when TIMESTAMP,
    updated_by VARCHAR(255),
    is_deleted BOOLEAN DEFAULT FALSE,
    deleted_when TIMESTAMP,
    deleted_by VARCHAR(255),
    CONSTRAINT FK_RESIDENT_TOUR FOREIGN KEY (resident_id) REFERENCES public.residents (id) ON DELETE CASCADE,
    CONSTRAINT FK_TYPE_OF_TOUR_TOUR FOREIGN KEY (type_of_tour_id) REFERENCES public.type_of_tour (id) ON DELETE CASCADE
);
CREATE TABLE public.users_tour (
    user_id BIGINT NOT NULL,
    tour_id BIGINT NOT NULL,
    CONSTRAINT FK_USERS_TOUR_USER FOREIGN KEY (user_id) REFERENCES public.users (id) ON DELETE CASCADE,
    CONSTRAINT FK_USERS_TOUR_TOUR FOREIGN KEY (tour_id) REFERENCES public.tour (id) ON DELETE CASCADE
);
CREATE TABLE public.reviews (
    id BIGSERIAL PRIMARY KEY,
    score INTEGER NOT NULL,
    description TEXT,
    users_id BIGINT NOT NULL,
    residents_id BIGINT NOT NULL,
    tour_id BIGINT NOT NULL,
    created_when TIMESTAMP,
    created_by VARCHAR(255),
    updated_when TIMESTAMP,
    updated_by VARCHAR(255),
    is_deleted BOOLEAN DEFAULT FALSE,
    deleted_when TIMESTAMP,
    deleted_by VARCHAR(255),
    CONSTRAINT FK_USER_REVIEW FOREIGN KEY (users_id) REFERENCES public.users (id) ON DELETE CASCADE,
    CONSTRAINT FK_RESIDENT_REVIEW FOREIGN KEY (residents_id) REFERENCES public.residents (id) ON DELETE CASCADE,
    CONSTRAINT FK_TOUR_REVIEW FOREIGN KEY (tour_id) REFERENCES public.tour (id) ON DELETE CASCADE
);

insert into role
values (1, 'ADMIN', 'Роль администратора'),
       (2, 'USER', 'Роль пользователя');

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user1', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user1@example.com', 'Иван', 'Иванов', '1990-01-01', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user2', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user2@example.com', 'Елена', 'Петрова', '1995-05-15', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user3', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user3@example.com', 'Алексей', 'Сидоров', '1988-09-20', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user4', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user4@example.com', 'Мария', 'Смирнова', '1982-07-10', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user5', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user5@example.com', 'Андрей', 'Волков', '1998-03-25', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('admin', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user6@example.com', 'Татьяна', 'Борисова', '1993-11-12', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user7', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user7@example.com', 'Сергей', 'Кузнецов', '1985-06-30', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user8', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user8@example.com', 'Анна', 'Федорова', '1996-12-05', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user9', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user9@example.com', 'Павел', 'Морозов', '1980-04-18', 'ADMIN', NOW());

INSERT INTO public.users (login, password, email, first_name, last_name, birth_date, created_by, created_when)
VALUES ('user10', '$2a$10$XDrDVL1dlInz7G6pOIA1F.ECeVcam0waXC90ZOfdi6S1YF13iCOPS', 'user10@example.com', 'Ольга', 'Новикова', '1977-10-08', 'ADMIN', NOW());


INSERT INTO public.users_role (user_id, role_id)
VALUES (1, 2);
INSERT INTO public.users_role (user_id, role_id)
VALUES (2, 2);
INSERT INTO public.users_role (user_id, role_id)
VALUES (3, 2);
INSERT INTO public.users_role (user_id, role_id)
VALUES (4, 2);
INSERT INTO public.users_role (user_id, role_id)
VALUES (5, 2);
INSERT INTO public.users_role (user_id, role_id)
VALUES (6, 1);
INSERT INTO public.users_role (user_id, role_id)
VALUES (7, 1);
INSERT INTO public.users_role (user_id, role_id)
VALUES (8, 1);
INSERT INTO public.users_role (user_id, role_id)
VALUES (9, 1);
INSERT INTO public.users_role (user_id, role_id)
VALUES (10, 1);


INSERT INTO public.type_of_tour (name, duration, price, photo, program, created_when, created_by)
VALUES ('Обзорная экскурсия', 1.5, 200, 'eksk_sezl.jpg', 'Программа обзорной экскурсии', NOW(), 'ADMIN');

INSERT INTO public.type_of_tour (name, duration, price, photo, program, created_when, created_by)
VALUES ('Обзорная экскурсия с посещением завода', 2.5, 350, 'eksk_zav.jpg', 'Программа экскурсии с посещением завода', NOW(), 'ADMIN');

INSERT INTO public.type_of_tour (name, duration, price, photo, program, created_when, created_by)
VALUES ('Обзорная экскурсия с посещением завода, Центра управления сетями', 3.5, 600, 'CUSL.png', 'Программа экскурсии экскурсия с посещением завода, Центра управления сетями', NOW(), 'ADMIN');

INSERT INTO public.type_of_tour (name, duration, price, photo, program, created_when, created_by)
VALUES ('Обзорная экскурсия с посещением завода, мастер-классом по росписи декоративной керамической тарелки с народным мастером России', 5, 1000, 'masterklassl.png', 'Программа обзорной экскурсии с посещением завода, мастер-классом по росписи декоративной керамической тарелки с народным мастером России', NOW(), 'ADMIN');


INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (1, 'Эгида', 'Один из лидеров рынка по производству пенополиуретана, тканей, клея и комплектующих для мягкой мебели. По статистике, каждый третий диван в России сделан из поролона от Эгиды.', 'Aegis.jpeg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (2, 'ФОНДИТАЛЬ', 'Итальянская промышленная группа, крупнейший мировой производитель алюминиевых радиаторов, а также производитель структурных компонентов для автомобильной промышленности. ', 'Fondital.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (3, 'Йокохама Р.П.З.', 'Один из ведущих мировых брендов по производству автомобильных шин.
С 2015 года компания – спонсор известного футбольного клуба "Челси".', 'Yokohama.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (4, 'Ви Фрай', 'Компания занимает первое место в стране по производству замороженного картофеля.
Ежедневно выпускает производит более 3,5 млн. порций вкуснейшего картофеля.', 'WeFry.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (5, 'РЭДАЛИТ Шлюмберже', 'ШЛЮМБЕРЖЕ – ведущий мировой поставщик технологий для комплексной оценки пласта, строительства скважин, управления добычей и переработки углеводородов.', 'SHlyumberzhe2.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (6, 'Гермес-Липецк', 'Предприятие является одним из крупнейших производителей отопительных, тепличных и паровых котлов по переданной в Россию немецкой технологии Viessmann.', 'Hermes-Lipetsk.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (7, 'ОБО Беттерманн Производство', 'Международный холдинг с более чем столетней историей.
Ассортимент продукции охватывает больше 30 000 наименований в семи производственных направлениях системных решений для электромонтажа.', 'sez.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (8, 'Липецкая Кофейная Компания', 'Крупная производственная компания по обжарке кофе с автоматизированной производственной линией полного цикла и инновационной системой повторения профиля обжарки.', 'LKK.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (9, 'Бекарт Липецк', 'Компания Бекарт является мировым лидером на рынке в области технологий преобразования стальной проволоки и нанесения покрытий.
Продукция компании широко применяется при строительстве таких объектов, как дорог, мостов, туннелей и шахт. ', 'Backart.jpg', NOW(), 'ADMIN');

INSERT INTO public.residents (id, name, description, photo, created_when, created_by)
VALUES (10, 'ЧСЗ-Липецк', 'Группа компаний ЧСЗ – один из крупнейших производителей высококачественной стеклотары в стране. Производственные мощности заводов группы позволяют выпускать до 1.5 млрд. стеклотары в год.', 'Emergency_situations.JPG', NOW(), 'ADMIN');



INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №1', 10, 18, '2024-05-01 09:00:00', 1, 1, 0, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №2', 8, 16, '2024-05-02 10:30:00', 2, 2, 1, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №3', 15, 21, '2024-05-03 14:00:00', 3, 3, 2, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №4', 12, 18, '2024-05-04 11:00:00', 4, 4, 0, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №5', 20, 25, '2024-05-05 09:30:00', 5, 3, 1, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №6', 8, 18, '2024-05-06 10:00:00', 6, 1, 0, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №7', 15, 20, '2024-05-07 13:00:00', 7, 2, 2, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №8', 10, 18, '2024-05-08 11:30:00', 8, 2, 0, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №9', 12, 20, '2024-05-09 12:00:00', 9, 4, 1, NOW(), 'ADMIN', 0);

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by, number_of_registered_people)
VALUES ('Тур №10', 18, 22, '2024-05-10 15:00:00', 10, 2, 2, NOW(), 'ADMIN', 0);



INSERT INTO public.users_tour (user_id, tour_id)
VALUES (1, 1);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (1, 2);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (2, 2);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (3, 3);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (4, 4);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (5, 5);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (1, 6);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (2, 7);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (3, 8);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (4, 9);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (5, 10);


INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №11', 10, 18, '2024-05-11 10:00:00', 1, 1, 1, NOW(), 'ADMIN');

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №12', 8, 16, '2024-05-12 11:00:00', 2, 2, 1, NOW(), 'ADMIN');

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №13', 15, 21, '2024-05-13 12:00:00', 3, 3, 1, NOW(), 'ADMIN');

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №14', 12, 20, '2024-05-14 13:00:00', 4, 4, 1, NOW(), 'ADMIN');

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №15', 18, 22, '2024-05-15 14:00:00', 5, 2, 1, NOW(), 'ADMIN');

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №16', 10, 18, '2024-05-16 15:00:00', 6, 1, 1, NOW(), 'ADMIN');

INSERT INTO public.tour (name, number_of_people, age_at, start_tour, resident_id, type_of_tour_id, status, created_when, created_by)
VALUES ('Тур №17', 15, 20, '2024-05-17 16:00:00', 7, 2, 1, NOW(), 'ADMIN');

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (1, 11);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (2, 12);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (3, 13);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (4, 14);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (5, 15);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (1, 16);

INSERT INTO public.users_tour (user_id, tour_id)
VALUES (2, 17);


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (4, 'Отличный тур, получили массу удовольствия!', 1, 1, 11, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (5, 'Было замечательно, рекомендую!', 1, 6, 16, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (3, 'Неплохо, но могло быть и лучше.', 2, 2, 2, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (5, 'Впечатления от тура только положительные!', 2, 2, 12, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (4, 'Хороший тур, хотелось бы больше экскурсий!', 2, 6, 16, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (5, 'Прекрасный тур, всем рекомендую!', 3, 3, 13, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (4, 'Хорошее времяпрепровождение, отличный гид!', 4, 4, 9, NOW(), 'ADMIN');


INSERT INTO public.reviews (score, description, users_id, residents_id, tour_id, created_when, created_by)
VALUES (3, 'Неплохой тур, но ожидал большего.', 5, 5, 5, NOW(), 'ADMIN');


