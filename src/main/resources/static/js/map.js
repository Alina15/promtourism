document.addEventListener("DOMContentLoaded", function() {
    var infoContainer = document.getElementById('info-container');
    var previousSelectedArea = null;
    var footer = document.querySelector('footer');


    if(infoContainer==null){
        updateFooterPosition();
    };

    function clearInfo() {
        infoContainer.innerHTML = '';

    }

    function addInfo(data) {
        clearInfo();
        if (!data || !data.name || !data.description) {
            console.error('Ошибка: получена некорректная информация');
            return;
        }
        var imgDiv = document.createElement('div');
        imgDiv.className = 'col-6 col-md-4';
        var img = document.createElement('img');
        img.className = 'img-border';
        img.src = '/residents/images/'+ data.photo;
        imgDiv.appendChild(img);

        var textDiv = document.createElement('div');
        textDiv.className = 'col-sm-6 col-md-8 g-4';
        var h1 = document.createElement('h1');
        h1.textContent = data.name;
        var p = document.createElement('p');
        p.textContent = data.description;

        var button1 = document.createElement('a');
        button1.className = 'btn btn-prom';
        button1.textContent = 'Подробнее о заводе';
        button1.href = '/residents/' + data.id;

        var button2 = document.createElement('a');
        button2.href = '/tours/addUserTour/' + data.id;
        button2.className = 'btn btn-prom';
        button2.textContent = 'Записаться на экскурсию';

        var rowDiv = document.createElement('div');
        rowDiv.className = 'row align-items-center';

        var innerDiv = document.createElement('div');
        innerDiv.className = 'col-6';
        innerDiv.appendChild(button1);
        rowDiv.appendChild(innerDiv);

        innerDiv = document.createElement('div');
        innerDiv.className = 'col-6';
        innerDiv.appendChild(button2);
        rowDiv.appendChild(innerDiv);

        textDiv.appendChild(h1);
        textDiv.appendChild(p);
        textDiv.appendChild(rowDiv);

        var rowDiv = document.createElement('div');
        rowDiv.className = 'row align-items-center';

        rowDiv.appendChild(imgDiv);
        rowDiv.appendChild(textDiv);

        infoContainer.appendChild(rowDiv);
        updateFooterPosition();
    }

    function updateFooterPosition() {
        if (footer) {
            footer.style.position = 'relative';
        }
    }

    function handleClick(event) {
        var areaId = event.target.getAttribute('data-area-id');

        if (previousSelectedArea) {
            previousSelectedArea.setAttribute('fill', 'white');
        }

        fetch('/rest/resident/getResident?id=' + areaId)
            .then(response => response.json())
            .then(data => {
                addInfo(data);
                var circle = document.getElementById(areaId);
                circle.setAttribute('fill', '#387ACA');


                previousSelectedArea = circle;
            })
            .catch(error => console.error('Ошибка при получении данных:', error));
    }


    var areas = document.getElementById('svg-object');
    areas.querySelectorAll('.area').forEach(function(area) {
        area.addEventListener('click', handleClick);
    });
});