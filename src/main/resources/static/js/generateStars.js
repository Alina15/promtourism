document.addEventListener("DOMContentLoaded", function() {
    var starsContainers = document.querySelectorAll(".starsContainer");
    starsContainers.forEach(function(container) {
        var initialScoreAttr = container.getAttribute("value");
        var initialScore = parseInt(initialScoreAttr);
            generateStars(container, initialScore);
    });
});

function generateStars(container, score) {
    container.innerHTML = "";

    for (var i = 0; i < 5; i++) {
        var star = document.createElement("span");
        if (i < score) {
            star.innerHTML = "&#9733;"; // Закрашенная звезда
        } else {
            star.innerHTML = "&#9734;"; // Пустая звезда
        }
        container.appendChild(star);
    }
}