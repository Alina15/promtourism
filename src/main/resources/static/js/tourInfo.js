function fetchTourInfo(selectedTourId) {
    fetch("/rest/tour/getTour?id=" + selectedTourId)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            var tourInfoElement = document.getElementById("tourInfo");
            var htmlContent = "<h2>" + data.name + "</h2>";
            htmlContent += "<p> Кол-во людей:" + data.numberOfPeople + "</p>";
            htmlContent += "<p> Ограничение по возрасту:" + data.ageAt + "</p>";
            htmlContent += "<p> Начало тура:" + data.startTourFormatter + "</p>";
            fetch("/rest/typeOfTour/getTypeOfTour?id=" + data.typeOfTourId)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(typeOfTour => {
                    htmlContent += "<p> Тип тура:" + typeOfTour.name + "</p>";
                    htmlContent += "<p> Цена тура:" + typeOfTour.price + "</p>";
                    if(data.numberOfRegisteredPeople < data.numberOfPeople){
                    htmlContent += " <div class='mt-3 row g-2'>";
                    htmlContent += "<button type='submit' class='btn btn-primary' onclick='validateForm()'>Добавить</button>";
                    htmlContent += "</div>";
                    }else{
                    htmlContent += "Места закончились";
                    }
                    tourInfoElement.innerHTML = htmlContent;
                })
                .catch(error => {
                    console.error('There has been a problem with your fetch operation:', error);
                });
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}

document.addEventListener("DOMContentLoaded", function () {
    document.getElementById("tourId").addEventListener("change", function () {
        var selectedTourId = this.value;
        fetchTourInfo(selectedTourId);
    });
});