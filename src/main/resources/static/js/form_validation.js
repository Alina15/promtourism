function validateForm() {
    'use strict'
    const forms = document.querySelectorAll('.needs-validation');
    const score = document.getElementById("score");
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }
                 if (score != null && (score.value < 1 || score.value > 5)) {
                     alert("Пожалуйста, введите корректную оценку от 1 до 5!");
                     event.preventDefault()
                     event.stopPropagation()
                     return false;
                 }
                form.classList.add('was-validated')
            }, false)
        })
}
