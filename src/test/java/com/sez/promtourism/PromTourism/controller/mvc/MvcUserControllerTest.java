package com.sez.promtourism.PromTourism.controller.mvc;


import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.UserDto;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.model.User;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MvcUserControllerTest extends CommonTestMVC{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TourRepository tourRepository;

    UserDto userDto1 = new UserDto("login1",
            "password1",
            "email@email.ru",
            "firstName1",
            "middleName1",
            "lastName1",
            LocalDate.now(),
            "1111111",
            "address1",
            new ArrayList<>(1),
            new ArrayList<>(),
            null);

    UserDto userDto2 = new UserDto("login2",
            "password2",
            "email2",
            "firstName2",
            "middleName2",
            "lastName2",
            LocalDate.now(),
            "phone2",
            "address2",
            new ArrayList<>(1),
            new ArrayList<>(1),
            null);
    @Test
    @Order(0)
    @DisplayName("Создание пользователя через MVC контроллер")
    @WithAnonymousUser
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию пользователя через MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/users/registration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .flashAttr("userForm", userDto1)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is3xxRedirection());
        log.info("Тест по созданию пользователя через MVC закончен");
    }
    @Test
    @DisplayName("Просмотр профиля через MVC контроллер")
    @Order(1)
    @WithUserDetails("login1")
    protected void getOne() throws Exception {
        log.info("Тест просмотра профиля MVC начат");
        User user = userRepository.findUserByLoginAndDeletedFalse(userDto1.getLogin());
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/users/profile/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("profile/viewUserProfile"))
                .andReturn();
    }

    @Test
    @DisplayName("Просмотр пользователей зарегестрированных на экскурсию через MVC контроллер")
    @Order(2)
    @WithUserDetails("login1")
    protected void listUsersByTour() throws Exception {
        log.info("Тест просмотра пользователей зарегестрированных на экскурсию MVC начат");
        Tour tour = tourRepository.findByName("Тур №3");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/users/listUsersByTour/" + tour.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("users/viewAllUsersByTour"))
                .andReturn();
    }
    @Test
    @Order(3)
    @DisplayName("Обновление пользователя через MVC контроллер")
    @WithAnonymousUser
    @Override
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению пользователя через MVC начат");
        User user = userRepository.findUserByLoginAndDeletedFalse(userDto1.getLogin());
        mvc.perform(MockMvcRequestBuilders.post("/users/profile/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("id", String.valueOf(user.getId()))
                        .flashAttr("userForm", userDto2)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is3xxRedirection());
        log.info("Тест по обновлению пользователя через MVC закончен");

    }
    @Test
    @DisplayName("Забыли пароль")
    @Order(4)
    @WithUserDetails("login1")
    protected void rememberPassword() throws Exception {
        log.info("Тест по сбросу пароля через MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/users/remember-password")
                        .flashAttr("changePasswordForm", userDto1)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"));
        log.info("Тест по сбросу пароля через MVC закончен");
    }

    @Test
    @DisplayName("Востановление пароля")
    @Order(5)
    @WithUserDetails("login1")
    protected void changePassword() throws Exception {
        userDto1.setChangePasswordToken(userRepository.findUserByLoginAndDeletedFalse(userDto1.getLogin()).getChangePasswordToken());
        userDto1.setPassword("12345");
        log.info("Тест по востановлению пароля через MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/users/change-password")
                        .param("uuid", String.valueOf(userDto1.getChangePasswordToken()))
                        .flashAttr("changePasswordForm", userDto1)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"));
        log.info("Тест по востановлению пароля через MVC закончен");
    }
}
