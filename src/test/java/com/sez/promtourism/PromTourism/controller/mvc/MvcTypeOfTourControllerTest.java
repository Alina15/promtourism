package com.sez.promtourism.PromTourism.controller.mvc;


import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.TypeOfTourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateTypeOfTourRequest;
import com.sez.promtourism.PromTourism.model.TypeOfTour;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.repository.TypeOfTourRepository;
import com.sez.promtourism.PromTourism.service.TypeOfTourService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MvcTypeOfTourControllerTest extends CommonTestMVC{
    @Autowired
    private TypeOfTourRepository typeOfTourRepository;
    @Autowired
    private TypeOfTourService typeOfTourService;


    CreateTypeOfTourRequest typeOfTourRequest = new CreateTypeOfTourRequest("TypeOfTourTitle1",
            20.0,
            200,
            "program");

    TypeOfTourDto typeOfTourDto = new TypeOfTourDto("TypeOfTourTitle2",
            20.0,
            200,
            "program",
            "photo");
    MockMultipartFile photoFile = new MockMultipartFile("photo", "photo.jpg", "image/jpeg", "Photo content".getBytes());
    MockMultipartFile photoNew = new MockMultipartFile("photoNew", "file2.jpg", "image/jpeg", "Another file content".getBytes());

    @Test
    @DisplayName("Просмотр всех типов тура через MVC контроллер")
    @Order(0)
    @WithAnonymousUser
    protected void listAll() throws Exception {
        log.info("Тест просмотра типов тура MVC начат");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/typeOfTours")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("typeOfTours/listAllTypeOfTours"))
                .andExpect(model().attributeExists("typeOfTours"))
                .andReturn();
    }
    @Test
    @Order(1)
    @DisplayName("Создание резидента через MVC контроллер")
    @WithUserDetails("admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест на создание резидента через MVC начат");


        mvc.perform(MockMvcRequestBuilders.multipart("/typeOfTours/add")
                        .file(photoFile)
                        .flashAttr("typeOfTourForm", typeOfTourRequest)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/typeOfTours"))
                .andExpect(redirectedUrl("/typeOfTours"));

        log.info("Тест на создание резидента через MVC завершен");
    }

    @Order(2)
    @Test
    @DisplayName("Обновление резидента через MVC контроллер")
    @WithUserDetails("admin")
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению резидента через MVC начат успешно");
        TypeOfTour typeOfTour = typeOfTourRepository.findByName(typeOfTourRequest.getName());
        mvc.perform(MockMvcRequestBuilders.multipart("/typeOfTours/update")
                        .file(photoNew)
                        .flashAttr("typeOfTourForm", typeOfTourDto)
                        .param("id", String.valueOf(typeOfTour.getId()))
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/typeOfTours"))
                .andExpect(redirectedUrl("/typeOfTours"));
        log.info("Тест по резидента автора через MVC закончен успешно");
    }
    @Test
    @DisplayName("Просмотр изображения типа тура через MVC контроллер")
    @Order(3)
    @WithAnonymousUser
    protected void getImage() throws Exception {
        log.info("Тест изображения типа тура MVC начат");
        TypeOfTour typeOfTour = typeOfTourRepository.findByName(typeOfTourDto.getName());
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/typeOfTours/images/"+typeOfTour.getPhoto())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }
    @Order(4)
    @Test
    @DisplayName("Софт удаление типа тура через MVC контроллер, тестирование 'residents/delete'")
    @WithUserDetails("admin")
    protected void deleteObject() throws Exception {
        log.info("Тест по soft удалению типа тура через MVC начат успешно");
        TypeOfTour typeOfTour = typeOfTourRepository.findByName(typeOfTourDto.getName());
        mvc.perform(MockMvcRequestBuilders.get("/typeOfTours/delete/{id}", typeOfTour.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/typeOfTours"))
                .andExpect(redirectedUrl("/typeOfTours"));
        log.info("Тест по soft удалению типа тура через MVC закончен успешно");
    }
    @Order(7)
    @Test
    @DisplayName("Софт восстановления типа тура через MVC контроллер, тестирование 'residents/delete'")
    @WithUserDetails("admin")
    protected void restoreObject() throws Exception {
        TypeOfTour typeOfTour = typeOfTourRepository.findByName(typeOfTourDto.getName());
        mvc.perform(MockMvcRequestBuilders.get("/typeOfTours/restore/{id}", typeOfTour.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/typeOfTours"))
                .andExpect(redirectedUrl("/typeOfTours"));
        log.info("Тест по soft восстановлению типа тура через MVC закончен успешно");
        typeOfTourService.delete(typeOfTour.getId());
    }
}
