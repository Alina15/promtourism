package com.sez.promtourism.PromTourism.controller.mvc;


import com.sez.promtourism.PromTourism.dto.AddOrDeleteTourDto;
import com.sez.promtourism.PromTourism.dto.TourDto;
import com.sez.promtourism.PromTourism.dto.create.CreateTourRequest;
import com.sez.promtourism.PromTourism.model.Tour;
import com.sez.promtourism.PromTourism.repository.TourRepository;
import com.sez.promtourism.PromTourism.service.TourService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;

import static com.sez.promtourism.PromTourism.model.Status.ASSIGNED;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Transactional
@Rollback(value = false)
public class MvcTourControllerTest extends CommonTestMVC{
    @Autowired
    private TourService tourService;
    @Autowired
    private TourRepository tourRepository;

    CreateTourRequest Tour_DTO_1 = new CreateTourRequest("TourTitle1",
            20,
            11,
            LocalDate.now().atStartOfDay(),
            1,
            1L,
            1L,
            ASSIGNED,
            new ArrayList<>(1));

    TourDto Tour_DTO_2 = new TourDto("TourTitle2",
            20,
            11,
            LocalDate.now().atStartOfDay(),
            1,
            1L,
            1L,
            ASSIGNED,
            new ArrayList<>(1),
            "01.01.2025 11:00");

    @Test
    @Order(0)
    @DisplayName("Создание тура через MVC контроллер")
    @WithUserDetails("admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию тура через MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/tours/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .flashAttr("tourForm", Tour_DTO_1)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is3xxRedirection());
        log.info("Тест по созданию тура через MVC закончен");
    }


    @Order(1)
    @Test
    @DisplayName("Обновление тура через MVC контроллер")
    @WithUserDetails("admin")
    @Override
    protected void updateObject() throws Exception {
        Tour tour = tourRepository.findByName(Tour_DTO_1.getName());
        mvc.perform(MockMvcRequestBuilders.post("/tours/update")
                        .param("id", String.valueOf(tour.getId()))
                        .flashAttr("tourForm", Tour_DTO_2)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
        log.info("Тест по обновлению тура через MVC закончен успешно");

    }

    @Test
    @Order(2)
    @DisplayName("Регистрация на тур через MVC контроллер")
    @WithUserDetails("user1")
    protected void addUserTour() throws Exception {
        AddOrDeleteTourDto addOrDeleteTourDto = new AddOrDeleteTourDto();
        Tour tour = tourRepository.findByName(Tour_DTO_2.getName());
        addOrDeleteTourDto.setTourId(tour.getId());
        log.info("Тест по регистрации на тур через MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/tours/addUserTour")
                        .contentType(MediaType.APPLICATION_JSON)
                        .flashAttr("addUserTourForm", addOrDeleteTourDto)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is3xxRedirection());
        log.info("Тест по регистрации на тур через MVC закончен");
    }


    @Order(3)
    @Test
    @DisplayName("Отмена тура у юзера через MVC контроллер")
    @WithUserDetails("user1")
    protected void deleteObject() throws Exception {
        log.info("Тест по отмене тура у юзера через MVC начат успешно");
        Tour tour = tourRepository.findByName(Tour_DTO_2.getName());
        mvc.perform(MockMvcRequestBuilders.get("/tours/deleteUserTour/{id}", String.valueOf(tour.getId()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        tourService.delete(tour.getId());
    }

}
