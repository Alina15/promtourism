package com.sez.promtourism.PromTourism.controller.mvc;


import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.create.CreateResidentRequest;
import com.sez.promtourism.PromTourism.service.ResidentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@Slf4j
@Transactional
@Rollback(value = false)
public class MvcResidentControllerTest extends CommonTestMVC{
    @Autowired
    private ResidentService residentService;
    @Value("${file.upload-dir}")
    private String uploadDir;
    CreateResidentRequest residentDto = new CreateResidentRequest("ResidentTitle",
            "description");
    ResidentDto residentDtoUpdate = new ResidentDto("ResidentTitleUpdate",
            "descriptionUpdate",
            "photo",
            "gallery");
    MockMultipartFile photoFile = new MockMultipartFile("photo", "photo.jpg", "image/jpeg", "Photo content".getBytes());
    MockMultipartFile file1 = new MockMultipartFile("files", "file1.jpg", "image/jpeg", "File content".getBytes());
    MockMultipartFile file2 = new MockMultipartFile("files", "file2.jpg", "image/jpeg", "Another file content".getBytes());
    MockMultipartFile photoNew = new MockMultipartFile("photoNew", "photo_new.jpg", "image/jpeg", "New photo content".getBytes());
    MockMultipartFile fileNew = new MockMultipartFile("newFiles", "file2.jpg", "image/jpeg", "Another file content".getBytes());

    @Test
    @Order(1)
    @DisplayName("Создание резидента через MVC контроллер")
    @WithUserDetails("admin")
   // @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест на создание резидента через MVC начат");


        mvc.perform(MockMvcRequestBuilders.multipart("/residents/add")
                        .file(file1)
                        .file(file2)
                        .file(photoFile)
                        .flashAttr("residentForm", residentDto)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/residents"))
                .andExpect(redirectedUrl("/residents"));

        log.info("Тест на создание резидента через MVC завершен");
    }


    @Order(2)
    @Test
    @DisplayName("Обновление резидента через MVC контроллер")
    @WithUserDetails("admin")
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению резидента через MVC начат успешно");


        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "name"));
        ResidentDto foundResidentForUpdate = residentService.searchResidents(residentDto.getName(), pageRequest).getContent().get(0);
        foundResidentForUpdate.setName(residentDtoUpdate.getName());

        mvc.perform(MockMvcRequestBuilders.multipart("/residents/update")
                        .file(photoNew)
                        .file(fileNew)
                        .flashAttr("residentForm", foundResidentForUpdate)
                        .param("id", String.valueOf(foundResidentForUpdate.getId()))
                        .param("deletedFiles", "deletedFile1.txt,deletedFile2.txt")
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/residents"))
                .andExpect(redirectedUrl("/residents"));
        log.info("Тест по резидента автора через MVC закончен успешно");
    }


    @Order(6)
    @Test
    @DisplayName("Софт удаление резидента через MVC контроллер, тестирование 'residents/delete'")
    @WithUserDetails("admin")
    protected void deleteObject() throws Exception {
        log.info("Тест по soft удалению резидента через MVC начат успешно");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "name"));
        ResidentDto foundResidentForDelete = residentService.searchResidents(residentDto.getName(), pageRequest).getContent().get(0);
        foundResidentForDelete.setDeleted(true);
        mvc.perform(MockMvcRequestBuilders.get("/residents/delete/{id}", foundResidentForDelete.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/residents"))
                .andExpect(redirectedUrl("/residents"));
        ResidentDto deletedAuthor = residentService.getOne(foundResidentForDelete.getId());
        assertTrue(deletedAuthor.isDeleted());
        log.info("Тест по soft удалению резидента через MVC закончен успешно");
    }
    @Order(7)
    @Test
    @DisplayName("Софт восстановления резидента через MVC контроллер, тестирование 'residents/delete'")
    @WithUserDetails("admin")
    protected void restoreObject() throws Exception {
        log.info("Тест по soft восстановлению резидента через MVC начат успешно");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "name"));
        ResidentDto foundResidentForDelete = residentService.searchResidentsIsAdmin(residentDto.getName(), pageRequest).getContent().get(0);
        foundResidentForDelete.setDeleted(false);
        mvc.perform(MockMvcRequestBuilders.get("/residents/restore/{id}", foundResidentForDelete.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/residents"))
                .andExpect(redirectedUrl("/residents"));
        ResidentDto deletedAuthor = residentService.getOne(foundResidentForDelete.getId());
        assertFalse(deletedAuthor.isDeleted());
        log.info("Тест по soft восстановлению резидента через MVC закончен успешно");
        residentService.delete(foundResidentForDelete.getId());
    }
    @Test
    @DisplayName("Просмотр всех резидентов через MVC контроллер")
    @Order(0)
    @WithAnonymousUser
    protected void listAll() throws Exception {
        log.info("Тест просмотра резидентов MVC начат");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/residents")
                        .param("page", "1")
                        .param("size", "6")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("residents/listAllResidents"))
                .andExpect(model().attributeExists("residents"))
                .andReturn();
    }
    @Test
    @DisplayName("Поиск резидентов через MVC контроллер")
    @Order(8)
    @WithAnonymousUser
    protected void search() throws Exception {
        log.info("Тест поиска резидентов MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/residents/search")
                        .param("page", "1")
                        .param("size", "6")
                        .param("name", "ResidentTitleUpdate")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("residents/listAllResidents"))
                .andExpect(model().attributeExists("residents"))
                .andReturn();
    }

    @Test
    @DisplayName("Просмотр одного резидента через MVC контроллер")
    @Order(3)
    @WithAnonymousUser
    protected void getOne() throws Exception {
        log.info("Тест просмотра одного резидента MVC начат");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "name"));
        ResidentDto foundResident = residentService.searchResidents(residentDto.getName(), pageRequest).getContent().get(0);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/residents/" + foundResident.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("residents/viewResident"))
                .andExpect(model().attributeExists("resident", "galleryFiles"))
                .andReturn();
    }
    @Test
    @DisplayName("Просмотр главного изображения резидента через MVC контроллер")
    @Order(4)
    @WithAnonymousUser
    protected void getImage() throws Exception {
        log.info("Тест главного изображения резидента MVC начат");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "name"));
        ResidentDto foundResident = residentService.searchResidents(residentDto.getName(), pageRequest).getContent().get(0);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/residents/images/"+foundResident.getPhoto())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

    @Test
    @DisplayName("Просмотр галереи изображений резидента через MVC контроллер")
    @Order(5)
    @WithAnonymousUser
    protected void getGallery() throws Exception {
        log.info("Тест галереи изображений резидента MVC начат");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "name"));
        ResidentDto foundResident = residentService.searchResidents(residentDto.getName(), pageRequest).getContent().get(0);

        String galleryPath = uploadDir + "/" + foundResident.getGallery();
        File gallery = new File(galleryPath);
        File[] galleryFiles = gallery.listFiles();

        assert galleryFiles != null;
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/residents/gallery/" + foundResident.getGallery()+"/"+galleryFiles[0].getName())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }
}
