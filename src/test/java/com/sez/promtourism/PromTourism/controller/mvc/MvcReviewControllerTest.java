package com.sez.promtourism.PromTourism.controller.mvc;


import com.sez.promtourism.PromTourism.dto.ResidentDto;
import com.sez.promtourism.PromTourism.dto.ReviewDto;
import com.sez.promtourism.PromTourism.dto.create.CreateReviewRequest;
import com.sez.promtourism.PromTourism.model.Review;
import com.sez.promtourism.PromTourism.model.User;
import com.sez.promtourism.PromTourism.repository.ReviewRepository;
import com.sez.promtourism.PromTourism.service.ResidentService;
import com.sez.promtourism.PromTourism.service.ReviewService;
import com.sez.promtourism.PromTourism.service.userdetails.CustomUserDetails;
import com.sez.promtourism.PromTourism.service.userdetails.CustomUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.CachingUserDetailsService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.http.RequestEntity.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MvcReviewControllerTest extends CommonTestMVC{
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ReviewRepository reviewRepository;

    CreateReviewRequest Review_DTO_1 = new CreateReviewRequest(4,
            "description1",
            1L,
            1L,
            1L
    );

    ReviewDto Review_DTO_2 = new ReviewDto(5,
            "description2",
            1L,
            1L,
            1L
    );

    @Test
    @Order(1)
    @DisplayName("Создание отзыва через MVC контроллер")
    @WithUserDetails("user1")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию отзыва через MVC начат");
        mvc.perform(MockMvcRequestBuilders.post("/reviews/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("id", "1")
                        .flashAttr("reviewForm", Review_DTO_1)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/reviews"))
                .andExpect(redirectedUrlTemplate("/reviews"))
                .andExpect(redirectedUrl("/reviews"));
        log.info("Тест по созданию отзыва через MVC закончен");
    }


    @Order(2)
    @Test
    @DisplayName("Обновление отзыва через MVC контроллер")
    @WithUserDetails("user1")
    protected void updateObject() throws Exception {
        Review review = reviewRepository.findByDescription(Review_DTO_1.getDescription());
        mvc.perform(MockMvcRequestBuilders.post("/reviews/update")
                        .param("id", String.valueOf(review.getId()))
                        .flashAttr("reviewForm", Review_DTO_2)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/reviews"));
        log.info("Тест по обновлению отзыва через MVC закончен успешно");

    }


    @Order(6)
    @Test
    @DisplayName("Софт удаление отзыва через MVC контроллер, тестирование 'reviews/delete'")
    @WithUserDetails("user1")
    protected void deleteObject() throws Exception {
        log.info("Тест по soft удалению отзыва через MVC начат успешно");
        Review review = reviewRepository.findByDescription(Review_DTO_2.getDescription());
        mvc.perform(MockMvcRequestBuilders.get("/reviews/delete/{id}", String.valueOf(review.getId()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/reviews"))
                .andExpect(redirectedUrl("/reviews"));
        ReviewDto deletedReview = reviewService.getOne(review.getId());
        assertTrue(deletedReview.isDeleted());
        log.info("Тест по soft удалению отзыва через MVC закончен успешно");
        reviewService.delete(deletedReview.getId());
    }

    @Test
    @DisplayName("Просмотр всех отзывов через MVC контроллер")
    @Order(0)
    @WithAnonymousUser
    protected void listAll() throws Exception {
        log.info("Тест просмотра отзывов MVC начат");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/reviews")
                        .param("page", "1")
                        .param("size", "9")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("reviews/listAllReviews"))
                .andExpect(model().attributeExists("reviews"))
                .andReturn();
    }
}
